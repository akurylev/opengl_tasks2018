#define PI 3.14159265
#include "cylinder.h"

MeshPtr makeCylinder (float radius, float radiusScale, float height, int numPolygons, bool rotateFlag) {
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  std::vector<glm::vec2> texcoords;

  float a = 2*radius*sin(PI/numPolygons);
  float angle = 180*(numPolygons-2)/numPolygons;
  float upperRadius;
  if (rotateFlag) {
    upperRadius = radius;
    radius *= radiusScale;
  }
  else {
    upperRadius = radius * radiusScale;
  }

  for (int i = 0; i < numPolygons; i++) {

    float fi = 2*PI/numPolygons*i;
    float fi1 = 2*PI/numPolygons*(i+1);

    float x1 = radius*cos(fi);
    float y1 = radius*sin(fi);
    float x2 = radius*cos(fi1);
    float y2 = radius*sin(fi1);

    float x1_up = upperRadius*cos(fi);
    float y1_up = upperRadius*sin(fi);
    float x2_up = upperRadius*cos(fi1);
    float y2_up = upperRadius*sin(fi1);

    // найдем из векторного произведения
    glm::vec3 normal = glm::cross(
            glm::vec3(x2 - x1, y2 - y1, 0),
            glm::vec3(0, 0, 1)
        );
    normal = normal / glm::length(normal);

    // Первый треугольник, образующий прямоугольник
    vertices.push_back(glm::vec3(x1, y1, 0));
    vertices.push_back(glm::vec3(x2, y2, 0));
    vertices.push_back(glm::vec3(x2_up, y2_up, height));

    normals.push_back(normal);
    normals.push_back(normal);
    normals.push_back(normal);

    texcoords.push_back(glm::vec2((float)(i) / numPolygons, 0.0));
    texcoords.push_back(glm::vec2((float)(i + 1) / numPolygons, 0.0));
    texcoords.push_back(glm::vec2((float)(i + 1) / numPolygons, 1.0));

    // Второй треугольник, образующий прямоугольник
    vertices.push_back(glm::vec3(x1, y1, 0));
    vertices.push_back(glm::vec3(x1_up, y1_up, height));
    vertices.push_back(glm::vec3(x2_up, y2_up, height));

    normals.push_back(normal);
    normals.push_back(normal);
    normals.push_back(normal);

    texcoords.push_back(glm::vec2((float)(i) / numPolygons, 0.0));
    texcoords.push_back(glm::vec2((float)(i) / numPolygons, 1.0));
    texcoords.push_back(glm::vec2((float)(i + 1) / numPolygons, 1.0));

    // верхняя крышка
    vertices.push_back(glm::vec3(x1_up, y1_up, height));
    vertices.push_back(glm::vec3(x2_up, y2_up, height));
    vertices.push_back(glm::vec3(0, 0, height));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

    // нижняя крышка
    vertices.push_back(glm::vec3(x1, y1, 0));
    vertices.push_back(glm::vec3(x2, y2, 0));
    vertices.push_back(glm::vec3(0, 0, 0));

    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));
    normals.push_back(glm::vec3(0.0, 0.0, -1.0));

    texcoords.push_back(glm::vec2(0.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 1.0));
    texcoords.push_back(glm::vec2(1.0, 0.0));

  }

  DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

  DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

  DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

  MeshPtr mesh = std::make_shared<Mesh>();
  mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
  mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
  mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);

  mesh->setPrimitiveType(GL_TRIANGLES);
  mesh->setVertexCount(vertices.size());

  // std::cout << "Цилиндр is created with " << numPolygons << " polygons\n";

  return mesh;
}
