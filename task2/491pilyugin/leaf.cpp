#include "leaf.h"

void addListCoord(std::vector<glm::vec3>& vertices, float a, float b, float c);

MeshPtr makeLeaf(float a, float b, float c) {

  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  std::vector<glm::vec2> texcoords;

  c /= 2;
  b /= 2;

  //верхний 1 треугольник
  addListCoord(vertices, 0, b, c);
  addListCoord(vertices, a, -b, c);
  addListCoord(vertices, a, b, c);

  normals.push_back(glm::vec3(0.0, 0.0, 1.0));
  normals.push_back(glm::vec3(0.0, 0.0, 1.0));
  normals.push_back(glm::vec3(0.0, 0.0, 1.0));

  texcoords.push_back(glm::vec2(0.0, 1.0));
  texcoords.push_back(glm::vec2(1.0, 0.0));
  texcoords.push_back(glm::vec2(1.0, 1.0));

  //верхний 2 треугольник
  addListCoord(vertices, 0, b, c);
  addListCoord(vertices, 0, -b, c);
  addListCoord(vertices, a, -b, c);

  normals.push_back(glm::vec3(0.0, 0.0, 1.0));
  normals.push_back(glm::vec3(0.0, 0.0, 1.0));
  normals.push_back(glm::vec3(0.0, 0.0, 1.0));

  texcoords.push_back(glm::vec2(0.0, 1.0));
  texcoords.push_back(glm::vec2(0.0, 0.0));
  texcoords.push_back(glm::vec2(1.0, 0.0));


  DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

  DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

  DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
  buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

  MeshPtr mesh = std::make_shared<Mesh>();
  mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
  mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
  mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
  mesh->setPrimitiveType(GL_TRIANGLES);
  mesh->setVertexCount(vertices.size());

  return mesh;
}

void addListCoord(std::vector<glm::vec3>& vert, float a, float b, float c) {
    glm::vec3 vec = glm::vec3(a, b, c);
    vert.push_back(vec);
}
