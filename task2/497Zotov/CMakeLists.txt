set(MYDIR task2/497Zotov)
include_directories(".")

set(SRC_FILES
    Application.cpp
    Camera.cpp
    Mesh.cpp
    ShaderProgram.cpp
    DebugOutput.cpp
    Main.cpp
    PerlinNoise.cpp
    Texture.cpp
)

set(HEADER_FILES
    Application.hpp
    Camera.hpp
    Mesh.hpp
    ShaderProgram.hpp
    DebugOutput.h
    PerlinNoise.h
    Texture.hpp
    LightInfo.hpp
)

set(SHADER_FILES
    shaders/texture.frag
    shaders/texture.vert
    shaders/heightColor.frag
    shaders/heightColor.vert
    shaders/marker.frag
    shaders/marker.vert
)

source_group("Shaders" FILES ${SHADER_FILES})

MAKE_TASK(497Zotov 2 "${SRC_FILES}")
