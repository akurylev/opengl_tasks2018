set(SRC_FILES
    Common.h
    Camera.h
    Mesh.h
    Mesh.cpp
    ShaderProgram.cpp
    ShaderProgram.h
    Camera.cpp
    DebugOutput.h
    DebugOutput.cpp
    Application.h
    Application.cpp
    Main.h
    Main.cpp
    LightInfo.h
    Texture.h
    Texture.cpp
)

MAKE_TASK(492Slyusarev 2 "${SRC_FILES}")

