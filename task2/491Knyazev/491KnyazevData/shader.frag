#version 330

uniform sampler2D diffuseTex;

struct LightInfo
{
	vec3 pos; //положение источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 La; //цвет и интенсивность окружающего света
	vec3 Ld; //цвет и интенсивность диффузного света
	vec3 Ls; //цвет и интенсивность бликового света
};
uniform LightInfo light;

uniform sampler2D snowTex;
uniform sampler2D grassTex;
uniform sampler2D sandTex;
uniform sampler2D gravelTex;
uniform sampler2D mapTex;
uniform sampler2D specularTex;

uniform vec4 lightDirCamSpace;


in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

in vec2 mapCoord; // Координаты для карты рельефа

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

void main()
{
    vec3 snowColor = texture(snowTex, texCoord).rgb;
    vec3 grassColor = texture(grassTex, texCoord).rgb;
    vec3 sandColor = texture(sandTex, texCoord).rgb;
    vec3 gravelColor = texture(gravelTex, texCoord).rgb;
    vec3 mapColor = texture(mapTex, mapCoord).rgb;

    float new_component = 0;
    if (all(greaterThan(mapColor, vec3(0.5))))
        new_component = 1;

    mat3 relief = mat3(snowColor, grassColor, sandColor);
    vec3 diffuseColor = (1 - new_component) * relief * mapColor + new_component * gravelColor;

	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

	float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)

	vec3 color = diffuseColor * (light.La + light.Ld * NdotL);

	if (NdotL > 0.0)
	{
		vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света

		float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
		blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
		color += light.Ls * Ks * blinnTerm;
	}

	fragColor = vec4(color, 1.0);
}
