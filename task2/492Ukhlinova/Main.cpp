#include "Application.h"
#include "Mesh.h"
#include "ShaderProgram.h"
#include "Texture.h"
#include "LightInfo.h"

#include <vector>
#include <sstream>
#include <iostream>

class SampleApplication : public Application 
{
public:
	MeshPtr _breatherSurface;
	MeshPtr _marker; //Маркер для источника света

	//Идентификатор шейдерной программы
    ShaderProgramPtr _shader;
    ShaderProgramPtr _markerShader;

    //Переменные для управления положением одного источника света
    float _lr = 10.0;
    float _phi = -5.0;
    float _theta = glm::pi<float>() * 0.2f;

    LightInfo _light;

    TexturePtr _marbleTexture1;
    TexturePtr _marbleTexture2;

    GLuint _sampler1;
    GLuint _sampler2;

	unsigned N = 500;
	double b = 0.4;

    float _animationSpeed = 0.2;
    float _textureMerge = 1.0;
    float _shift = 0.0;
    bool stopAnimation = false;

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<OrbitCameraMover>();

		//Создаем меш с Breather Surface
		_breatherSurface = makeBreatherSurface(b, N);
		//_breatherSurface = makeSphere(5.0f);
		_breatherSurface->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		_marker = makeSphere(1.0f);

		//Создаем шейдерную программу        
		_shader = std::make_shared<ShaderProgram>("492UkhlinovaData/texture_shader.vert", "492UkhlinovaData/texture_shader.frag");
		_markerShader = std::make_shared<ShaderProgram>("492UkhlinovaData/marker.vert", "492UkhlinovaData/marker.frag");

		//=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

		//=========================================================
        //Загрузка и создание текстур
		_marbleTexture1 = loadTexture("492UkhlinovaData/marble.jpg");
		_marbleTexture2 = loadTexture("492UkhlinovaData/marble2.jpg");

		//=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
	    glGenSamplers(1, &_sampler1);
	    glSamplerParameteri(_sampler1, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler1, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	    glSamplerParameteri(_sampler1, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);

	    glGenSamplers(1, &_sampler2);
	    glSamplerParameteri(_sampler2, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler2, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	    glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_S, GL_MIRRORED_REPEAT);
	    glSamplerParameteri(_sampler2, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);

		if (ImGui::Begin("Breather Surface", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			ImGui::Checkbox("Stop Animation", &stopAnimation);
		    ImGui::SliderFloat("Texture", &_textureMerge, 0.0f, 1.0f);
		    ImGui::SliderFloat("Animation speed", &_animationSpeed, 0.0f, 0.5f);

            ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
            ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
            ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

            ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
            ImGui::SliderFloat("phi", &_phi, -5.0f, 2.0f * glm::pi<float>());
            ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());

		}
		ImGui::End();

	}

	void handleKey(int key, int scancode, int action, int mods) override
    {

        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
        	if (key == GLFW_KEY_SPACE) {
        		stopAnimation = !stopAnimation;
        	}	
        }
	}

	void draw() override
	{
		Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//Режим заливки полигона
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        //Подключаем шейдер	
		_shader->use();

		//Устанавливаем общие юниформ-переменные
		_shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		_light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

        _shader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _shader->setVec3Uniform("light.La", _light.ambient);
        _shader->setVec3Uniform("light.Ld", _light.diffuse);
        _shader->setVec3Uniform("light.Ls", _light.specular);


        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
	    glBindSampler(0, _sampler1);
	    _marbleTexture1->bind();
	    _shader->setIntUniform("diffuseTex1", 0);

	    glActiveTexture(GL_TEXTURE1);  //текстурный юнит 1
	    glBindSampler(1, _sampler2);
	    _marbleTexture2->bind();
	    _shader->setIntUniform("diffuseTex2", 1);
        
  		if (!stopAnimation) {
			_shift = glfwGetTime() * _animationSpeed;
	  		_textureMerge = abs(cos(_shift));
  		}
        
        _shader->setFloatUniform("shift", _shift);
        _shader->setFloatUniform("textureMerge", _textureMerge);

        {
			_shader->setMat4Uniform("modelMatrix", _breatherSurface->modelMatrix());
			_shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _breatherSurface->modelMatrix()))));

			_breatherSurface->draw();
		}

		{
			_markerShader->use();

			_markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
	        _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
	        _marker->draw();
	    }

		glBindSampler(0, 0);
        glUseProgram(0);
	}
};

int main(int argc, char** argv)
{
	SampleApplication app;
	app.start();

    return 0;
}
