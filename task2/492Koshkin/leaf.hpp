#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

#include <glm/gtx/rotate_vector.hpp>

#include "mesh.hpp"
#include <vector>
#include <iostream>

MeshPtr make_leaf(float length, float width, float height);
