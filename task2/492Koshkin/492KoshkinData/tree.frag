#version 330
/**
Использование текстуры в качестве коэффициента отражения света
*/

uniform sampler2D diffuse_tex;
uniform sampler2D normal_map_tex;

uniform mat3 light_dir_to_cam_space;

struct LightInfo
{
	vec3 direction; // Положение источника света в мировой системе координат
	vec3 La; // Цвет и интенсивность окружающего света
	vec3 Ld; // Цвет и интенсивность диффузного света
	vec3 Ls; // Цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 vertex_normal; // Нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec3 vertex_position; // Координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 vertex_tex_coord; // Текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

mat3 cotangent_frame(vec3 normal, vec3 pos, vec2 uv) {
    vec3 dp1 = dFdx(pos);
    vec3 dp2 = dFdy(pos);
    vec2 duv1 = dFdx(uv);
    vec2 duv2 = dFdy(uv);
    vec3 dp2perp = cross(dp2, normal);
    vec3 dp1perp = cross(normal, dp1);
    vec3 T = dp2perp * duv1.x + dp1perp * duv2.x;
    vec3 B = dp2perp * duv1.y + dp1perp * duv2.y;
    float invmax = inversesqrt(max(dot(T, T), dot(B, B)));
    return mat3(T * invmax, B * invmax, normal);
}

void main() {
	vec3 diffuse_color = texture(diffuse_tex, vertex_tex_coord).rgb;
    vec3 normal_map = texture(normal_map_tex, vertex_tex_coord).rgb;
	vec3 normalized_normal = normalize(vertex_normal); // Нормализуем нормаль после интерполяции

    mat3 tbn = cotangent_frame(normalized_normal, vertex_position, vertex_tex_coord);
    vec3 real_normal = normalize(tbn * (normal_map * 2.0 - 1.0));

	vec3 view_direction = normalize(-vertex_position); // Направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	vec3 light_direction = -light_dir_to_cam_space * light.direction; // Направление на источник света	
	float diffuse_intensity = max(dot(real_normal, light_direction), 0.0); // Скалярное произведение (косинус)
	vec3 color = diffuse_color * (light.La + light.Ld * diffuse_intensity);

	if (diffuse_intensity > 0.0) {			
		vec3 half_direction = normalize(light_direction + view_direction); // Биссектриса между направлениями на камеру и на источник света
		float specular_intensity = max(pow(dot(real_normal, half_direction), shininess), 0.0); // Интенсивность бликового освещения по Блинну				
		color += light.Ls * Ks * specular_intensity;
	}
	fragColor = vec4(color, 1.0);
}
