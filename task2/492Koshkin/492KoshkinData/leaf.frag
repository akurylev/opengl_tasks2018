#version 330
/**
Использование текстуры в качестве коэффициента отражения света
*/

uniform sampler2D diffuse_tex;
uniform mat3 light_dir_to_cam_space;

struct LightInfo
{
	vec3 direction; // Положение источника света в мировой системе координат
	vec3 La; // Цвет и интенсивность окружающего света
	vec3 Ld; // Цвет и интенсивность диффузного света
	vec3 Ls; // Цвет и интенсивность бликового света
};
uniform LightInfo light;

in vec3 vertex_normal; // Нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec3 vertex_position; // Координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 vertex_tex_coord; // Текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

void main()
{
	vec4 diffuse_color = texture(diffuse_tex, vertex_tex_coord);
	vec3 real_normal = normalize(vertex_normal); //нормализуем нормаль после интерполяции

	vec3 view_direction = normalize(-vertex_position); // Направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	vec3 light_direction = -light_dir_to_cam_space * light.direction; // Направление на источник света	
	float diffuse_intensity = max(dot(real_normal, light_direction), 0.0); //скалярное произведение (косинус)
	vec3 color = diffuse_color.rgb * (light.La + light.Ld * diffuse_intensity);

	if (diffuse_intensity > 0.0)
	{			
		vec3 half_direction = normalize(light_direction + view_direction); // Биссектриса между направлениями на камеру и на источник света
		float specular_intensity = max(pow(dot(real_normal, half_direction), shininess), 0.0); // Интенсивность бликового освещения по Блинну				
		color += light.Ls * Ks * specular_intensity;
	}
	fragColor = vec4(color, diffuse_color.a * 0.8);
}
