#version 330

uniform mat3 lightDirCamSpace;
uniform sampler2D diffuseTex;

struct LightInfo
{
        vec3 dir; //положение источника света в системе координат мира 
        vec3 La; //цвет и интенсивность окружающего света
        vec3 Ld; //цвет и интенсивность диффузного света
        vec3 Ls; //цвет и интенсивность бликового света
};

uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec3 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 1024.0;

void main()
{
        vec4 diffuseColor = texture(diffuseTex, texCoord);

        vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
        vec3 viewDirection = normalize(-posCamSpace); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))

        vec3 lightDirCam = -lightDirCamSpace * light.dir; //направление на источник света

        float NdotL = max(dot(normal, lightDirCam), 0.0); //скалярное произведение (косинус)

        vec3 color = diffuseColor.rgb * (light.La + light.Ld * NdotL);

        if (NdotL > 0.0)
        {
                vec3 halfVector = normalize(lightDirCam + viewDirection); //биссектриса между направлениями на камеру и на источник света

                float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну                              
                blinnTerm /= 2;
                blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
                color += light.Ls * Ks * blinnTerm;
        }

        fragColor = vec4(color, 0.75 * diffuseColor.a);
}
