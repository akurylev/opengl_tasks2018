#pragma once

#include <map>
#pragma once

#include <map>
#include <vector>
#include <string>
#include <utility>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>


struct State {
    glm::vec3 angles;
    glm::vec4 position;
    float stepDistance;
    float rotateAngle;
    float invert;
    float bottomRadius;
};


class LSystem {
    float initialRotateAngle;
    float initialStepDistance;
    float distanceScale;
    float angleScale;
    float radiusScale;
    float initialBottomRadius;
    std::map<char, std::string> rules;
    std::string initialString;
    std::string currentString;
public:
    LSystem() = default;
    LSystem(
        const std::map<char, std::string>& rules,
        std::string initialString,
        float rotateAngle,
        float stepDistance,
        float distanceScale,
        float angleScale,
        float radiusScale,
        float initialBottomRadius
    );
    void executeIterations(uint16_t numIterations);
    std::vector<std::pair<glm::vec3, float>> getPoints();
protected:
    glm::mat4 getStep(const State& state);
    void pushPosition(const State& state, std::vector<std::pair<glm::vec3, float>>& result);
    void UpdateState(State& state);
};
