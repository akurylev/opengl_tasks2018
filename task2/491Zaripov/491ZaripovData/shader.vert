#version 330

uniform mat4 modelMatrix; //из локальной в мировую
uniform mat4 viewMatrix; //из мировой в систему координат камеры
uniform mat4 projectionMatrix; //из системы координат камеры в усеченные координаты

uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты
layout(location = 3) in vec2 vertexMapCoord; //текстурные координаты для карты рельефа

out vec3 normalCamSpace; // Нормаль в системе координат камеры
out vec4 posCamSpace; // Координаты вершины в системе координат камеры
out vec2 texCoord; // Текстурные координаты
out vec2 mapCoord;

void main()
{
    texCoord = vertexTexCoord;
    mapCoord = vertexMapCoord;
    posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
    normalCamSpace = normalize(normalToCameraMatrix * vertexNormal);
    gl_Position = projectionMatrix * posCamSpace;
}
