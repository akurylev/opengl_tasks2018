#include "SurfaceApplication.h"

SurfaceApplication::SurfaceApplication() = default;

void SurfaceApplication::makeScene() {

    Application::makeScene();

    mesh_ = calculateSurface(5.f, 200U, 4, 0.9);
    mesh_->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

    camera_handler_ = std::make_shared<FirstPersonCameraHandler>(mesh_);
    shader_program_ = std::make_shared<ShaderProgram>("493LatypovData/shader.vert",
                                                      "493LatypovData/shader.frag");

    light_info_.ambient = glm::vec3(0.2, 0.2, 0.2);
    light_info_.diffuse = glm::vec3(0.8, 0.8, 0.8);
    light_info_.specular = glm::vec3(0.2, 0.2, 0.2);
    light_info_.position = glm::vec3(glm::cos(phi_) * glm::cos(theta_), glm::sin(phi_) * glm::cos(theta_),
                                     glm::sin(theta_));

    snow_ = loadTexture("493LatypovData/snow.jpg");
    grass_ = loadTexture("493LatypovData/grass.jpg");
    sand_ = loadTexture("493LatypovData/sand.jpg");
    stone_ = loadTexture("493LatypovData/stone.jpg");
    map_ = loadTexture("493LatypovData/map.jpg");

    auto specular_color_props = glm::vec4(2, 1, 0.5, 0.5);
    shader_program_->setVec4Uniform("specular_color_props", specular_color_props);

    glGenSamplers(1, &sampler_);
    glSamplerParameteri(sampler_, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(sampler_, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glSamplerParameteri(sampler_, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(sampler_, GL_TEXTURE_WRAP_T, GL_REPEAT);
}

void SurfaceApplication::updateGUI() {
    Application::updateGUI();

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("Zufar Latypov 493", nullptr, ImGuiWindowFlags_AlwaysAutoResize)) {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

        if (ImGui::CollapsingHeader("Light")) {
            ImGui::ColorEdit3("ambient", glm::value_ptr(light_info_.ambient));
            ImGui::ColorEdit3("diffuse", glm::value_ptr(light_info_.diffuse));
            ImGui::ColorEdit3("specular", glm::value_ptr(light_info_.specular));

            ImGui::SliderFloat("phi", &phi_, 0.0f, 2.0f * glm::pi<float>());
            ImGui::SliderFloat("theta", &theta_, 0.0f, glm::pi<float>());
        }
    }
    ImGui::End();
}

void SurfaceApplication::draw() {
    Application::draw();

    int width, height;
    glfwGetFramebufferSize(window_, &width, &height);
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    shader_program_->use();
    shader_program_->setMat4Uniform("view_matrix", camera_.view_matrix);
    shader_program_->setMat4Uniform("projection_matrix", camera_.projection_matrix);
    shader_program_->setMat4Uniform("model_matrix", mesh_->GetModelMatrix());
    shader_program_->setMat3Uniform("normal_to_camera_matrix",
                                    glm::transpose(
                                            glm::inverse(glm::mat3(camera_.view_matrix * mesh_->GetModelMatrix()))));

    light_info_.position = glm::vec3(glm::cos(phi_) * glm::cos(theta_), glm::sin(phi_) * glm::cos(theta_),
                                     glm::sin(theta_));


    glm::vec4 light_dir_cam_space = glm::vec4(camera_.view_matrix * glm::vec4(light_info_.position, 0.0));
    shader_program_->setVec4Uniform("light_dir_cam_space", light_dir_cam_space);

    shader_program_->setVec3Uniform("light.Dir", light_info_.position);
    shader_program_->setVec3Uniform("light.La", light_info_.ambient);
    shader_program_->setVec3Uniform("light.Ld", light_info_.diffuse);
    shader_program_->setVec3Uniform("light.Ls", light_info_.specular);

    glActiveTexture(GL_TEXTURE0);
    glBindSampler(0, sampler_);
    snow_->bind();
    shader_program_->setIntUniform("snow_texture", 0);

    glActiveTexture(GL_TEXTURE0 + 1);
    grass_->bind();
    shader_program_->setIntUniform("grass_texture", 1);

    glActiveTexture(GL_TEXTURE0 + 2);
    sand_->bind();
    shader_program_->setIntUniform("sand_texture", 2);

    glActiveTexture(GL_TEXTURE0 + 3);
    stone_->bind();
    shader_program_->setIntUniform("stone_texture", 3);

    glActiveTexture(GL_TEXTURE0 + 4);
    map_->bind();
    shader_program_->setIntUniform("map_texture", 4);

    mesh_->draw();
    glBindSampler(0, 0);
    glUseProgram(0);
}