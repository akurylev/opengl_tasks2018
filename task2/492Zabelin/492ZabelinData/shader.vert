#version 330

uniform mat4 toCameraMatrix;
uniform mat3 normalToCameraMatrix;
uniform mat4 projectionMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexTextureCoords;

// в системе координат камеры
out vec3 normal;

// в системе координат камеры
out vec3 position;
out vec2 textureCoords;

void main() {
    textureCoords = vertexTextureCoords;
    vec4 _position_vec4 = toCameraMatrix * vec4(vertexPosition, 1);
    position = _position_vec4.xyz;
    normal = normalToCameraMatrix * vertexNormal;
    gl_Position = projectionMatrix * _position_vec4;
}
