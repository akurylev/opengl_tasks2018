#include "TreeApplication.h"
#include "Parallelepiped.h"
#include <string>
#include <iostream>


glm::vec3 getVec3(const glm::vec4& a) {
    return glm::vec3(a.x, a.y, a.z);
}


void printVec(const glm::vec3& a, std::string name = "") {
    std::cout << name << " " << a.x << " " << a.y << " " << a.z << std::endl;
}

void TreeApplication::setupLSystem(
    LSystem& system,
    uint16_t numIterations
) {
    system.executeIterations(numIterations);
    conesParams = system.getConesParams();
}

void TreeApplication::setLeafParams(
    float thicknessBound,
    float width,
    float length,
    size_t leafNumber
) {
    gen = std::mt19937(111);
    angleDistribution = std::uniform_real_distribution<float>(-M_PI, M_PI);
    spaceDistribution = std::uniform_real_distribution<float>(0, 1);

    leafThicknessBound = thicknessBound;
    leafWidth = width;
    leafLength = length;
    leafNum = leafNumber;
}

void TreeApplication::addLeaf(
    const ConeParams& params,
    bool drawOnTop
) {
    glm::vec3 tmp(0, 0, 0);
    glm::vec3 differ = params.secondPoint - params.firstPoint;
    if (differ.x)
        tmp.z = 1;
    else
        tmp.x = 1;
    glm::vec3 direction = glm::normalize(glm::cross(tmp, differ));

    float alpha = 0.01;
    if (!drawOnTop)
        alpha = spaceDistribution(gen);
    glm::vec3 barkAxis = glm::normalize(params.secondPoint - params.firstPoint);
    glm::vec3 petioleShift = barkAxis * float(leafWidth * 0.2);

    glm::vec3 firstPoint = alpha * params.firstPoint + (1 - alpha) * params.secondPoint;

    float realThickness = params.thickness * (alpha + params.thicknessScale * (1 - alpha));
    realThickness -= leafLength * 0.15;
    firstPoint += direction * realThickness;

    glm::vec3 secondPoint = firstPoint + direction * leafLength;
    glm::vec3 normal = glm::normalize(glm::cross(barkAxis, secondPoint - firstPoint));
    glm::vec3 center = alpha * params.firstPoint + (1 - alpha) * params.secondPoint;
    float angleAroundBark = angleDistribution(gen);
    float angleAroundSelfAxis = angleDistribution(gen);

    std::vector<glm::vec3> leaf = {
        firstPoint + barkAxis * (leafWidth / 2),
        firstPoint - barkAxis * (leafWidth / 2),
        secondPoint - barkAxis * (leafWidth / 2),
        firstPoint + barkAxis * (leafWidth / 2),
        secondPoint - barkAxis * (leafWidth / 2),
        secondPoint + barkAxis * (leafWidth / 2)
    };
    std::vector<glm::vec2> leafTextureCoords = {
        glm::vec2(0, 0),
        glm::vec2(0, 1),
        glm::vec2(1, 1),
        glm::vec2(0, 0),
        glm::vec2(1, 1),
        glm::vec2(1, 0),
    };

    glm::mat4 modelMatrix;
    modelMatrix = glm::translate(glm::mat4(1.0f), center);
    modelMatrix = glm::rotate(modelMatrix, angleAroundBark, barkAxis);
    modelMatrix = glm::translate(modelMatrix, firstPoint - center + petioleShift);
    modelMatrix = glm::rotate(modelMatrix, angleAroundSelfAxis, direction);
    modelMatrix = glm::translate(modelMatrix, -firstPoint - petioleShift);

    normal = getVec3((glm::transpose(glm::inverse(modelMatrix)) * glm::vec4(normal, 1)));
    for (int i = 0; i < 6; ++i) {
        leaves.push_back(swapAxis(getVec3(modelMatrix * glm::vec4(leaf[i], 1))));
        leavesNormal.push_back(swapAxis(normal));
        leavesTextureCoords.push_back(leafTextureCoords[i]);
    }
}

void TreeApplication::makeScene()
{
    Application::makeScene();
    cones.reserve(conesParams.size());
    for (const auto& params: conesParams) {
        cones.push_back(getCone(params));
        if (params.thickness < leafThicknessBound)
            for (size_t leafInd = 0; leafInd < leafNum; ++leafInd)
                addLeaf(params, leafInd < 2);
    }
    DataBufferPtr verteciesBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    verteciesBuffer->setData(leaves.size() * sizeof(float) * 3, leaves.data());

    DataBufferPtr normalsBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    normalsBuffer->setData(leavesNormal.size() * sizeof(float) * 3, leavesNormal.data());

    DataBufferPtr textureBuffer = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    textureBuffer->setData(leavesTextureCoords.size() * sizeof(float) * 2, leavesTextureCoords.data());

    leavesMesh = std::make_shared<Mesh>();
    leavesMesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, verteciesBuffer);
    leavesMesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, normalsBuffer);
    leavesMesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, textureBuffer);
    leavesMesh->setPrimitiveType(GL_TRIANGLES);
    leavesMesh->setVertexCount(leaves.size());
    leavesMesh->setModelMatrix(glm::mat4(1));

    // Инициализируем параметры материалов
    glm::vec3 brown(1, 92. / 117, 72. / 117);
    barkMaterial.Ka = brown;
    barkMaterial.Kd = brown;
    barkMaterial.Ks = brown / float(2.);
    leafMaterial.Ka = glm::vec3(1);
    leafMaterial.Kd = glm::vec3(1);
    leafMaterial.Ks = glm::vec3(1. / 5);

    //Создаем шейдерную программу
    shader = std::make_shared<ShaderProgram>(
        "492ZabelinData/shader.vert",
        "492ZabelinData/shader.frag"
    );

    leafShader = std::make_shared<ShaderProgram>(
        "492ZabelinData/shader.vert",
        "492ZabelinData/leafShader.frag"
    );

    //Инициализация значений переменных освщения
    _lightAmbientColor = glm::vec3(0.2, 0.2, 0.2);
    _lightDiffuseColor = glm::vec3(0.8, 0.8, 0.8);
    _lightSpecularColor = glm::vec3(1, 1, 1);

    // Загрузка и создание текстур
    barkTexture = loadTexture("492ZabelinData/wood_texture.jpg");
    normalMapTexture = loadTexture("492ZabelinData/wood_normal_map.jpg");
    leafTexture = loadTexture("492ZabelinData/leaf_alphamasked.png");

    // Инициализация сэмплеров
    glGenSamplers(1, &barkSampler);

    glSamplerParameteri(barkSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(barkSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(barkSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(barkSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glSamplerParameterf(barkSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4.0f);

    glGenSamplers(1, &normalMapSampler);
    glSamplerParameteri(normalMapSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(normalMapSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(normalMapSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(normalMapSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glSamplerParameterf(normalMapSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4.0f);

    glGenSamplers(1, &leafSampler);
    glSamplerParameteri(leafSampler, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glSamplerParameteri(leafSampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(leafSampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(leafSampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glSamplerParameterf(leafSampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4.0f);
}

void TreeApplication::updateGUI()
{
    Application::updateGUI();

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

        if (ImGui::CollapsingHeader("Light"))
        {
            ImGui::ColorEdit3("light ambient", glm::value_ptr(_lightAmbientColor));
            ImGui::ColorEdit3("light diffuse", glm::value_ptr(_lightDiffuseColor));
            ImGui::ColorEdit3("light specular", glm::value_ptr(_lightSpecularColor));

            ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
            ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
        }

        if (ImGui::CollapsingHeader("Bark material"))
        {
            ImGui::ColorEdit3("bark mat ambient", glm::value_ptr(barkMaterial.Ka));
            ImGui::ColorEdit3("bark mat diffuse", glm::value_ptr(barkMaterial.Kd));
            ImGui::ColorEdit3("bark mat specular", glm::value_ptr(barkMaterial.Ks));
        }

        if (ImGui::CollapsingHeader("Leaf material"))
        {
            ImGui::ColorEdit3("leaf mat ambient", glm::value_ptr(leafMaterial.Ka));
            ImGui::ColorEdit3("leaf mat diffuse", glm::value_ptr(leafMaterial.Kd));
            ImGui::ColorEdit3("leaf mat specular", glm::value_ptr(leafMaterial.Ks));
        }
    }
    ImGui::End();
}


void TreeApplication::draw()
{
    Application::draw();

    //Получаем размеры экрана (окна)
    int width, height;
    glfwGetFramebufferSize(_window, &width, &height);

    //Устанавливаем порт вывода на весь экран (окно)
    glViewport(0, 0, width, height);

    //Очищаем порт вывода (буфер цвета и буфер глубины)
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Устанавливаем шейдер.
    shader->use();

    //Устанавливаем общие юниформ-переменные
    glm::vec3 lightDir = glm::vec3(
        glm::cos(_phi) * glm::cos(_theta),
        glm::sin(_phi) * glm::cos(_theta),
        glm::sin(_theta)
    );
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);

    shader->setVec3Uniform("light.dir", lightDir);
    shader->setVec3Uniform("light.La", _lightAmbientColor);
    shader->setVec3Uniform("light.Ld", _lightDiffuseColor);
    shader->setVec3Uniform("light.Ls", _lightSpecularColor);


    shader->setVec3Uniform("material.Ka", barkMaterial.Ka);
    shader->setVec3Uniform("material.Kd", barkMaterial.Kd);
    shader->setVec3Uniform("material.Ks", barkMaterial.Ks);

    // Почему-то если barkUnit = 0, то GUI не отображается
    GLuint barkUnit = 3;
    glBindTextureUnit(barkUnit, barkTexture->texture());
    glBindSampler(barkUnit, barkSampler);
    shader->setIntUniform("diffuseTexture", barkUnit);

    GLuint normalMapUnit = 1;
    glBindTextureUnit(normalMapUnit, normalMapTexture->texture());
    glBindSampler(normalMapUnit, normalMapSampler);
    shader->setIntUniform("normalMapTexture", normalMapUnit);

    glEnable(GL_BLEND);
    glBlendFunc(GL_ONE, GL_ZERO);

    //Рисуем дерево
    for (const auto& cone: cones) {
        glm::mat4 toCameraMatrix = _camera.viewMatrix * cone->modelMatrix();
        glm::mat3 normalToCameraMatrix = glm::transpose(glm::inverse(glm::mat3(toCameraMatrix)));
        shader->setMat4Uniform("toCameraMatrix", toCameraMatrix);
        shader->setMat3Uniform("normalToCameraMatrix", normalToCameraMatrix);
        cone->draw();
    }

    //Устанавливаем шейдер.
    leafShader->use();

    //Устанавливаем общие юниформ-переменные
    leafShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);
    leafShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);

    leafShader->setVec3Uniform("light.dir", lightDir);
    leafShader->setVec3Uniform("light.La", _lightAmbientColor);
    leafShader->setVec3Uniform("light.Ld", _lightDiffuseColor);
    leafShader->setVec3Uniform("light.Ls", _lightSpecularColor);

    leafShader->setVec3Uniform("material.Ka", leafMaterial.Ka);
    leafShader->setVec3Uniform("material.Kd", leafMaterial.Kd);
    leafShader->setVec3Uniform("material.Ks", leafMaterial.Ks);

    GLuint leafUnit = 2;
    glBindSampler(leafUnit, leafSampler);
    glBindTextureUnit(leafUnit, leafTexture->texture());
    leafShader->setIntUniform("diffuseTexture", leafUnit);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glm::mat3 normalToCameraMatrix = glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix)));
    leafShader->setMat4Uniform("toCameraMatrix", _camera.viewMatrix);
    leafShader->setMat3Uniform("normalToCameraMatrix", normalToCameraMatrix);
    leavesMesh->draw();
}
