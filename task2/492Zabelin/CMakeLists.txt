set(SRC_FILES
    common/Application.cpp
        common/DebugOutput.cpp
    common/Camera.cpp
    common/Mesh.cpp
    common/ShaderProgram.cpp
    common/Texture.cpp
    Cone.cpp
    Main.cpp
    TreeApplication.cpp
    LSystem.cpp
    Parallelepiped.cpp
)

set(HEADER_FILES
    TreeApplication.h
    LSystem.h
    Cone.h
    Parallelepiped.h
    common/Application.hpp
        common/DebugOutput.h
    common/LightInfo.hpp
    common/Texture.hpp
    common/Camera.hpp
    common/Mesh.hpp
    common/ShaderProgram.hpp
)

include_directories(common)

MAKE_TASK(492Zabelin 2 "${SRC_FILES}")
