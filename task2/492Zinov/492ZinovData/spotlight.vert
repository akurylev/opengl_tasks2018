/**
  Пофрагментное освещение точечным источником света с учетом затухания. Окружающий, диффузный и бликовый свет.
 */

#version 330

struct LightInfo
{
    vec3 pos; //положение источника света в мировой системе координат (для точечного источника)
    float coneAngle;
    vec3 coneDirection;
    vec3 La; //коэффициент отражения окружающего света
    vec3 Ld; //коэффициент отражения диффузного света
};
uniform LightInfo light;
uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalToCameraMatrix;

layout(location = 0) in vec3 vertexPosition; //координаты вершины в локальной системе координат
layout(location = 1) in vec3 vertexNormal; //нормаль в локальной системе координат
layout(location = 2) in vec2 vertexTexCoord; //текстурные координаты вершины

out vec4 posCamSpace; //координаты вершины в системе координат камеры
out vec3 normal;
out vec4 lightPosCamSpace;
out vec2 texCoord; //текстурные координаты

void main()
{
    texCoord = vertexTexCoord;
    normal = vertexNormal;
    posCamSpace = viewMatrix * modelMatrix * vec4(vertexPosition, 1.0); //преобразование координат вершины в систему координат камеры
    lightPosCamSpace = viewMatrix * vec4(light.pos, 1.0); //положение источника света - из мировой в систему координат камеры

    gl_Position = projectionMatrix * posCamSpace;
}

