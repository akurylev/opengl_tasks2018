//
// Created by Alexander Boymel on 08.03.18.
//
#include "Maze.h"

const float EPS = 0.15;  // not to see through walls


MazeApplication::MazeApplication(std::string _mazeFilename, float _cellSize, bool _faceCamera) {
    cellSize = _cellSize;
    faceCamera = _faceCamera;
    cellCount = 0;
    std::ifstream fin(_mazeFilename);

    fin >> width >> height;
    maze.resize(width);
    for (size_t i = 0; i < width; ++i) {
        maze[i].resize(height);
        for (size_t j = 0; j < height; ++j) {
            fin >> maze[i][j];
        }
    }
    fin.close();

    normalMap = { // normals inside
            {'l', glm::vec3(-1.0, 0.0, 0.0)},
            {'r', glm::vec3(1.0, 0.0, 0.0)},
            {'n', glm::vec3(0.0, -1.0, 0.0)},
            {'f', glm::vec3(0.0, 1.0, 0.0)},
            {'b', glm::vec3(0.0, 0.0, 1.0)},
            {'t', glm::vec3(0.0, 0.0, -1.0)}
    };
}


/*
walls:
  l
n   f
  r
b - bottom
t - top
l - left
r - right
n - near
f - far
*/
std::vector<char> MazeApplication::getWalls(int i, int j) {
    std::vector<char> walls;
    walls.push_back('b');
    walls.push_back('t');

    int cur = maze[i][j];

    if (cur == 0) {
        if ((i > 0) && (maze[i - 1][j] != cur))
            walls.push_back('l');
        if ((i < width - 1) && (maze[i + 1][j] != cur))
            walls.push_back('r');
        if ((j > 0) && (maze[i][j - 1] != cur))
            walls.push_back('n');
        if ((j < height - 1) && (maze[i][j + 1] != cur))
            walls.push_back('f');

        if (i == 0)
            walls.push_back('l');
        if (i == width - 1)
            walls.push_back('r');
        if (j == 0)
            walls.push_back('n');
        if (j == height - 1)
            walls.push_back('f');
    }

    return walls;
}


void MazeApplication::initGL() {
    Application::initGL();
    glGetIntegerv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &_maxAvailableAnisotropy);
    _maxAnisotropy = std::min(_maxAnisotropy, _maxAvailableAnisotropy);
}


void MazeApplication::makeScene() {
    Application::makeScene();

    for (size_t i = 0; i < width; ++i) {
        for (size_t j = 0; j < height; ++j) {
            std::vector<char> walls = getWalls(i, j);
            if ((walls.size() > 0)) {
                addCell(i, j, walls);
            }
        }
    }

    faceCameraPtr = std::make_shared<MazeFreeCameraMover>(this);
    orbitCameraPtr = std::make_shared<OrbitCameraMover>();
    if (faceCamera) {
        _cameraMover = faceCameraPtr;
    } else {
        _cameraMover = orbitCameraPtr;
    }

    marker = makeSphere(0.1f);
    spot = makeSphere(0.1f);

    //=========================================================
    //Загрузка и создание текстур
    textures.push_back(loadTexture("492BoymelData/images/188.jpg"));
    textures.push_back(loadTexture("492BoymelData/images/granite.jpg"));
    textures.push_back(loadTexture("492BoymelData/images/brick.jpg"));

    textures.push_back(loadTexture("492BoymelData/images/lava.jpg"));
    textures.push_back(loadTexture("492BoymelData/images/173.jpg"));
    textures.push_back(loadTexture("492BoymelData/images/sky.jpg"));


    normals.push_back(loadTexture("492BoymelData/images/188_norm.jpg"));
    normals.push_back(loadTexture("492BoymelData/images/granite_norm.jpg"));
    normals.push_back(loadTexture("492BoymelData/images/brick_norm.jpg"));

    normals.push_back(loadTexture("492BoymelData/images/lava_norm.jpg"));
    normals.push_back(loadTexture("492BoymelData/images/173_norm.jpg"));
    normals.push_back(loadTexture("492BoymelData/images/sky_norm.jpg"));

    //=========================================================

    for (size_t i = 0; i < 10; ++i) {
        int x, y;
        std::vector<char> walls;
        while (true) {
            x = rand() % width;
            y = rand() % height;

            if (maze[x][y] == 0) {
                walls = getWalls(x, y);
                if (walls.size() > 2)
                    break;
            }
        }

        char type = walls[2 + rand() % (walls.size() - 2)];
        pictures.push_back(makePicture(x, y, type));
        pictureTextIndexes.push_back(3 + rand() % (textures.size() - 3));
    }

    shader = std::make_shared<ShaderProgram>("492BoymelData/shaders/texture.vert", "492BoymelData/shaders/texture.frag");
    markerShader = std::make_shared<ShaderProgram>("492BoymelData/shaders/marker.vert", "492BoymelData/shaders/marker.frag");

    //=========================================================
    //Инициализация значений переменных освщения
    _light[0].position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
    _light[0].ambient = glm::vec3(0.2, 0.2, 0.2);
    _light[0].diffuse = glm::vec3(0.8, 0.8, 0.8);
    _light[0].specular = glm::vec3(1.0, 1.0, 1.0);

    _light[1].position = faceCameraPtr->getPos();
    _light[1].ambient = glm::vec3(0.2, 0.0, 0.0);
    _light[1].diffuse = glm::vec3(0.5, 0.1, 0.1);
    _light[1].specular = glm::vec3(1.0, 1.0, 1.0);

    //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
    glGenSamplers(1, &_sampler);
    glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);

    glSamplerParameterf(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, _maxAnisotropy);
}


void MazeApplication::updateGUI() {
    Application::updateGUI();

    ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
    if (ImGui::Begin("Maze", NULL, ImGuiWindowFlags_AlwaysAutoResize))
    {
        ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

        if (ImGui::CollapsingHeader("Light"))
        {
            ImGui::ColorEdit3("ambient", glm::value_ptr(_light[0].ambient));
            ImGui::ColorEdit3("diffuse", glm::value_ptr(_light[0].diffuse));
            ImGui::ColorEdit3("specular", glm::value_ptr(_light[0].specular));

            ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
            ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
            ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
        }
        bool anisotropyChanged = ImGui::SliderInt("Max anisotropy", &_maxAnisotropy, 1, _maxAvailableAnisotropy);
        if (anisotropyChanged) {
            glSamplerParameteri(_sampler, GL_TEXTURE_MAX_ANISOTROPY_EXT, _maxAnisotropy);
        }
    }
    ImGui::End();
}


void MazeApplication::draw() {
    Application::draw();

    int _width, _height;
    glfwGetFramebufferSize(_window, &_width, &_height);

    glViewport(0, 0, _width, _height);

    //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Подключаем шейдер
    shader->use();

    //Устанавливаем общие юниформ-переменные
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    _light[0].position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
    _light[1].position = faceCameraPtr->getPos();

    for (unsigned int i = 0; i < LightNum; i++)
    {
        std::ostringstream str;
        str << "light[" << i << "]";

        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light[i].position, 1.0));

        shader->setVec3Uniform(str.str() + ".pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        shader->setVec3Uniform(str.str() + ".La", _light[i].ambient);
        shader->setVec3Uniform(str.str() + ".Ld", _light[i].diffuse);
        shader->setVec3Uniform(str.str() + ".Ls", _light[i].specular);
    }

    {
        glBindSampler(0, _sampler);
        glBindSampler(1, _sampler);
    }
    shader->setIntUniform("diffuseTex", 0);
    shader->setIntUniform("normalTex", 1);

    //Рисуем
    for (int i = 0; i < cellCount; ++i) {
        shader->setMat4Uniform("modelMatrix", cellsTranslations[i]);
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * cellsTranslations[i]))));

        glActiveTexture(GL_TEXTURE0);
        textures[textureIndexes[i]]->bind();

        glActiveTexture(GL_TEXTURE1);
        normals[textureIndexes[i]]->bind();

        cellsMesh[i]->draw();
    }

    for (int i = 0; i < pictures.size(); ++i) {
        shader->setMat4Uniform("modelMatrix", glm::mat4(1.0f));
        shader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * glm::mat4(1.0f)))));

        glActiveTexture(GL_TEXTURE0);
        textures[pictureTextIndexes[i]]->bind();

        glActiveTexture(GL_TEXTURE1);
        normals[pictureTextIndexes[i]]->bind();

        pictures[i]->draw();
    }

    //Рисуем маркеры для всех источников света
    {
        markerShader->use();

        markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light[0].position));
        markerShader->setVec4Uniform("color", glm::vec4(_light[0].diffuse, 1.0f));
        marker->draw();
    }

    {
        markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light[1].position));
        markerShader->setVec4Uniform("color", glm::vec4(_light[1].diffuse, 1.0f));
        spot->draw();
    }

    //Отсоединяем сэмплер и шейдерную программу
    glBindSampler(0, 0);
    glUseProgram(0);
}


void MazeApplication::handleKey(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        if (key == GLFW_KEY_ESCAPE)
        {
            glfwSetWindowShouldClose(_window, GL_TRUE);
        }
        if (key == GLFW_KEY_F) {
//            _cameraMover = std::make_shared<FreeCameraMover>();
            _cameraMover = faceCameraPtr;
            std::cout << "face" << std::endl;
        }
        if (key == GLFW_KEY_O) {
//            _cameraMover = std::make_shared<OrbitCameraMover>();
            _cameraMover = orbitCameraPtr;
            std::cout << "orbit" << std::endl;
        }
    }
    _cameraMover->handleKey(_window, key, scancode, action, mods);
}


void MazeApplication::fillCellVertices(int i, int j, std::vector<glm::vec3> & cellVertices) {
    float half = cellSize / 2;
    float x = cellSize * i + half;
    float y = cellSize * j + half;

    /*
     * 0  1
     * 3  2
     */
    cellVertices.emplace_back(glm::vec3(x - half, y - half, 0.));
    cellVertices.emplace_back(glm::vec3(x - half, y + half, 0.));
    cellVertices.emplace_back(glm::vec3(x + half, y + half, 0.));
    cellVertices.emplace_back(glm::vec3(x + half, y - half, 0.));

    cellVertices.emplace_back(glm::vec3(x - half, y - half, cellSize));
    cellVertices.emplace_back(glm::vec3(x - half, y + half, cellSize));
    cellVertices.emplace_back(glm::vec3(x + half, y + half, cellSize));
    cellVertices.emplace_back(glm::vec3(x + half, y - half, cellSize));

    std::cout << "New Cube " << i << ',' << j << std::endl;
    std::cout << "All cubes" << std::endl;
}


std::vector<glm::vec3> MazeApplication::getWallVertices(char type,
                                                        const std::vector<glm::vec3> & cellVertices) {
    std::vector<glm::vec3> wallVertices;
    switch (type) {
        case 'b':
            wallVertices.push_back(cellVertices[3]);
            wallVertices.push_back(cellVertices[2]);
            wallVertices.push_back(cellVertices[1]);
            wallVertices.push_back(cellVertices[0]);
            break;
        case 't':
            wallVertices.push_back(cellVertices[4]);
            wallVertices.push_back(cellVertices[5]);
            wallVertices.push_back(cellVertices[6]);
            wallVertices.push_back(cellVertices[7]);
            break;
        case 'l':
            wallVertices.push_back(cellVertices[4]);
            wallVertices.push_back(cellVertices[5]);
            wallVertices.push_back(cellVertices[1]);
            wallVertices.push_back(cellVertices[0]);
            break;
        case 'r':
            wallVertices.push_back(cellVertices[6]);
            wallVertices.push_back(cellVertices[7]);
            wallVertices.push_back(cellVertices[3]);
            wallVertices.push_back(cellVertices[2]);
            break;
        case 'n':
            wallVertices.push_back(cellVertices[0]);
            wallVertices.push_back(cellVertices[3]);
            wallVertices.push_back(cellVertices[7]);
            wallVertices.push_back(cellVertices[4]);
            break;
        case 'f':
            wallVertices.push_back(cellVertices[5]);
            wallVertices.push_back(cellVertices[6]);
            wallVertices.push_back(cellVertices[2]);
            wallVertices.push_back(cellVertices[1]);
            break;
    }
    return wallVertices;
}


//MeshPtr MazeApplication::makeCellMesh(int i, int j, std::vector<char> walls) {
MeshPtr MazeApplication::makeCellMesh(int i, int j, char type) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> cellVertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;

    std::vector<glm::vec3> tangents;

    fillCellVertices(i, j, cellVertices);

//    for (char type: walls) {

    {
        std::vector<glm::vec3> wallVertices = getWallVertices(type, cellVertices);
        //1 triangle
        vertices.push_back(wallVertices[0]);
        vertices.push_back(wallVertices[1]);
        vertices.push_back(wallVertices[2]);
        //2 triangle
        vertices.push_back(wallVertices[2]);
        vertices.push_back(wallVertices[3]);
        vertices.push_back(wallVertices[0]);

        for (size_t k = 0; k < 6; ++k) {
            normals.push_back(normalMap[type]);
        }

        texCoords.emplace_back(glm::vec2(1.0, 1.0));
        texCoords.emplace_back(glm::vec2(0.0, 1.0));
        texCoords.emplace_back(glm::vec2(0.0, 0.0));

        texCoords.emplace_back(glm::vec2(0.0, 0.0));
        texCoords.emplace_back(glm::vec2(1.0, 0.0));
        texCoords.emplace_back(glm::vec2(1.0, 1.0));

        if ((type == 'l') or (type == 'r')) {
            for (size_t _ = 0; _ < 6; ++_) {
                tangents.push_back(glm::vec3(0, 1, 0));
            }
        }
        if ((type == 'n') or (type == 'f')) {
            for (size_t _ = 0; _ < 6; ++_) {
                tangents.push_back(glm::vec3(1, 0, 0));
            }
        }
        if ((type == 't') or (type == 'b')) {
            for (size_t _ = 0; _ < 6; ++_) {
                tangents.push_back(glm::vec3(-1, 0, 0));
            }
        }

    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texCoords.size() * sizeof(float) * 2, texCoords.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(texCoords.size() * sizeof(float) * 3, tangents.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}


void MazeApplication::addCell(int i, int j, const std::vector<char> & walls) {
    size_t index = 0;
    for (char type: walls) {
        ++cellCount;
//        cellsMesh.push_back(makeCellMesh(i, j, walls));
        cellsMesh.push_back(makeCellMesh(i, j, type));
        if (type == 'b') {
            index = 0;
        } else if (type == 't') {
            index = 1;
        } else {
            index = 2;
        }
        textureIndexes.push_back(index);
        cellsTranslations.push_back(glm::mat4(1.0f));
    }
}


bool MazeApplication::checkCell(int i, int j, float x, float y, int pos_i, int pos_j) {
    if ((i < 0) || (i >= width) || (j < 0) || (j >= height) || maze[i][j] == 1)
        return true;
    if ((i == pos_i) && (j == pos_j))
        return true;

    float xBorder = cellSize * (i);
    if (pos_i > i)
        xBorder += cellSize;

    float yBorder = cellSize * j;
    if (pos_j > j)
        yBorder += cellSize;

    return sqrt(pow(x - xBorder, 2) * pow(i - pos_i, 2) + pow(y - yBorder, 2) * pow(j - pos_j, 2)) > EPS;
}


bool MazeApplication::possibleMove(glm::vec3 move) {
    if ((move[2] < 0) || (move[2] > cellSize))
        return true;
    else {
        int i = static_cast<int>(move[0] / cellSize);
        int j = static_cast<int>(move[1] / cellSize);

        float x = move[0];
        float y = move[1];

        if (x < 0)
            i -= 1;
        if (y < 0)
            j -= 1;
        for (int new_i = i - 1; new_i <= i + 1; ++new_i)
            for (int new_j = j - 1; new_j <= j + 1; ++new_j) {
                if (!checkCell(new_i, new_j, x, y, i, j))
                    return false;
            }
        return true;
    }

}


float MazeApplication::getCellSize() {
    return cellSize;
}


//-------------------------------------------------

MazeFreeCameraMover::MazeFreeCameraMover(MazeApplication* mazeApp) :
        CameraMover(),
        _pos(-2.0f, -2.0f, mazeApp->getCellSize() / 2),
        maze(mazeApp)
{
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void MazeFreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void MazeFreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void MazeFreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeFreeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        if (maze->possibleMove(_pos + forwDir * speed * static_cast<float>(dt)))
            _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        if (maze->possibleMove(_pos - forwDir * speed * static_cast<float>(dt)))
            _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        if (maze->possibleMove(_pos - rightDir * speed * static_cast<float>(dt)))
            _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        if (maze->possibleMove(_pos + rightDir * speed * static_cast<float>(dt)))
            _pos += rightDir * speed * static_cast<float>(dt);
    }

    //-----------------------------------------
    _pos[2] = maze->getCellSize() / 2;

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}

glm::vec3 MazeFreeCameraMover::getPos(){
    return _pos;
}


MeshPtr MazeApplication::makePicture(int i, int j, char type) {
    float half = cellSize / 2;
    float x = cellSize * i + half;
    float y = cellSize * j + half;

    float diff = cellSize / 50;
    half += diff;
    std::vector<glm::vec3> cellVertices;

    cellVertices.emplace_back(glm::vec3(x - half, y - half, cellSize * 0.4));
    cellVertices.emplace_back(glm::vec3(x - half, y + half, cellSize * 0.4));
    cellVertices.emplace_back(glm::vec3(x + half, y + half, cellSize * 0.4));
    cellVertices.emplace_back(glm::vec3(x + half, y - half, cellSize * 0.4));

    cellVertices.emplace_back(glm::vec3(x - half, y - half, cellSize * 0.7));
    cellVertices.emplace_back(glm::vec3(x - half, y + half, cellSize * 0.7));
    cellVertices.emplace_back(glm::vec3(x + half, y + half, cellSize * 0.7));
    cellVertices.emplace_back(glm::vec3(x + half, y - half, cellSize * 0.7));

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;
    std::vector<glm::vec3> tangents;

    std::vector<glm::vec3> wallVertices = getWallVertices(type, cellVertices);
    //1 triangle
    vertices.push_back(wallVertices[0]);
    vertices.push_back(wallVertices[1]);
    vertices.push_back(wallVertices[2]);
    //2 triangle
    vertices.push_back(wallVertices[2]);
    vertices.push_back(wallVertices[3]);
    vertices.push_back(wallVertices[0]);

    for (size_t k = 0; k < 6; ++k) {
        normals.push_back(normalMap[type]);
    }

    texCoords.emplace_back(glm::vec2(1.0, 1.0));
    texCoords.emplace_back(glm::vec2(0.0, 1.0));
    texCoords.emplace_back(glm::vec2(0.0, 0.0));

    texCoords.emplace_back(glm::vec2(0.0, 0.0));
    texCoords.emplace_back(glm::vec2(1.0, 0.0));
    texCoords.emplace_back(glm::vec2(1.0, 1.0));

    if ((type == 'l') or (type == 'r')) {
        for (size_t _ = 0; _ < 6; ++_) {
            tangents.push_back(glm::vec3(0, 1, 0));
        }
    }
    if ((type == 'n') or (type == 'f')) {
        for (size_t _ = 0; _ < 6; ++_) {
            tangents.push_back(glm::vec3(1, 0, 0));
        }
    }
    if ((type == 't') or (type == 'b')) {
        for (size_t _ = 0; _ < 6; ++_) {
            tangents.push_back(glm::vec3(1, 0, 0));
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texCoords.size() * sizeof(float) * 2, texCoords.data());

    DataBufferPtr buf3 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf3->setData(normals.size() * sizeof(float) * 3, tangents.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setAttribute(3, 3, GL_FLOAT, GL_FALSE, 0, 0, buf3);

    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}