//
// Created by nikolay on 14.03.18.
//

#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <sstream>
#include <vector>



class SampleApplication : public Application
{
public:
    MeshPtr _paraboloid;

    ShaderProgramPtr _shader;

    float _N = 50.0;
    float _radius = 2.0;

    void makeScene() override
    {
        Application::makeScene();

        _paraboloid = makeParaboloid(_radius, static_cast<unsigned int>(_N));
        _paraboloid->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));


        //=========================================================
        //Инициализация шейдеров

        _shader = std::make_shared<ShaderProgram>("492OsipovData/shader.vert", "492OsipovData/shader.frag");

    }

    void updateGUI() override
    {
        Application::updateGUI();

        if (glfwGetKey(_window, GLFW_KEY_MINUS) == GLFW_PRESS)
        {
            _N = _N - 5.0f;
            if (_N < 5.0)
            {
                _N = 5.0;
            }
            _paraboloid = makeParaboloid(_radius, static_cast<unsigned int>(_N));
        }

        if (glfwGetKey(_window, GLFW_KEY_EQUAL) == GLFW_PRESS)
        {
            _N = _N + 5.0f;
            _paraboloid = makeParaboloid(_radius, static_cast<unsigned int>(_N));
        }

        Application::update();
    }

    void draw() override
    {
        Application::draw();

        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем шейдер
        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        _shader->setMat4Uniform("modelMatrix", _paraboloid->modelMatrix());
        _paraboloid->draw();
    }
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}