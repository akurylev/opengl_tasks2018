//
// Created by avk on 14.03.18.
//

#include "../include/MazeApplication.h"
#include "Mesh.hpp"

void MazeApplication::makeScene() {
    //=========================================================
    //Инициализация шейдеров

    _shader = std::make_shared<ShaderProgram>("496KaryaminData/simple.vert", "496KaryaminData/simple.frag");

    //=========================================================

    createFloor();
    createWalls();
    createCeil();

    Application::makeScene();
}


MazeApplication::MazeApplication() : Application(std::make_shared<OrbitCameraMover>()) {
    _maze = createTestMaze();
    _cameraMover = std::make_shared<MazeWalkerCamera>(_maze);
    _anotherCamera = std::make_shared<OrbitCameraMover>();
}

void MazeApplication::draw() {
    Application::draw();
    int width, height;

    glfwGetFramebufferSize(_window, &width, &height);
    glViewport(0, 0, width, height);

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    _shader->use();

    _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    _shader->setMat4Uniform("modelMatrix", _floor->modelMatrix());
    _floor->draw();

//    int counter = 0;
    for (MeshPtr mesh: _walls) {
        _shader->setMat4Uniform("modelMatrix", mesh->modelMatrix());
//        std::cout << ++counter << std::endl;
        mesh->draw();
    }
    if (_drawCeil) {
        _shader->setMat4Uniform("modelMatrix", _ceil->modelMatrix());
        _ceil->draw();
    }
}

void MazeApplication::createFloor() {
    _floor = makeRectangle(3, 3, X_AXIS, Y_AXIS, Z_AXIS);
//    _floor = makeParallelepiped(3, 3, 3);
    _floor->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, 1.5f, 0.0f)));
}

void MazeApplication::createCeil() {
    _ceil = makeRectangle(3, 3, X_AXIS, Y_AXIS, Z_AXIS);
//    _floor = makeParallelepiped(3, 3, 3);
    _ceil->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(1.5f, 1.5f, 1.0f)));
}

void MazeApplication::createWalls() {
    for (const Wall& w: _maze->getWalls()) {
        std::cout << "WALL: "
                  << "(" << w.px.x << "," << w.px.y << "," << w.px.z << ")"
                  << "(" << w.py.x << "," << w.py.y << "," << w.py.z << ")"
                  << "[" << (w.py.x + w.py.y)/2 << "]"
                  << std::endl;

        float a = std::abs(w.px.x - w.py.x);
        float b = std::abs(w.px.y - w.py.y);

        MeshPtr wall;

        if (a == 0) {
            wall = makeRectangle(std::max(a,b), 1, Y_AXIS, Z_AXIS, X_AXIS);
        } else if (b == 0) {
            wall = makeRectangle(std::max(a,b), 1, X_AXIS, Z_AXIS, Y_AXIS);
        } else {
            std::cerr << "something went wrong" << std::endl;
        }
        wall->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3((w.px.x + w.py.x)/2, (w.px.y + w.py.y)/2, 0.5f)));
        _walls.emplace_back(std::move(wall));
    }
}

void MazeApplication::handleMouseMove(double x, double y) {
    Application::handleMouseMove(x, y);
}

void MazeApplication::handleKey(int key, int scancode, int action, int mods) {
    if (action == GLFW_PRESS && key == GLFW_KEY_F1) {
        _changeCameraMover();
    }

    Application::handleKey(key, scancode, action, mods);
}

void MazeApplication::_changeCameraMover() {
    std::swap(_cameraMover, _anotherCamera);
}
