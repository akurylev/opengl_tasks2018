#include "Application.h"
#include "Main.h"
#include "Mesh.h"
#include "ShaderProgram.h"

#include <vector>
#include <iostream>

class SampleApplication : public Application 
{
public:
	MeshPtr _moebiusStrip;

	std::vector<ShaderProgramPtr> _shaders;

	unsigned detail = 100;
	int polygonMode = GL_FILL;
	int shaderIndex = 0;

	void makeScene() override
	{
		Application::makeScene();

		_cameraMover = std::make_shared<OrbitCameraMover>();

		//������� ��� � ������ ̸�����
		_moebiusStrip = makeMoebiusStrip(0.5f, detail);
		_moebiusStrip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));

		//������� ��������� ���������        
		_shaders.push_back(std::make_shared<ShaderProgram>("492SlyusarevData/shaderNormal.vert", "492SlyusarevData/shader.frag"));
		_shaders.push_back(std::make_shared<ShaderProgram>("492SlyusarevData/shaderNormalWithAxes.vert", "492SlyusarevData/shader.frag"));

		detail = 100;
	}

	void updateGUI() override
	{
		Application::updateGUI();

		ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

			ImGui::RadioButton("filled", &polygonMode, GL_FILL);
			ImGui::RadioButton("grid", &polygonMode, GL_LINE);

			ImGui::Text("Axes:");

			ImGui::RadioButton("OFF", &shaderIndex, 0);
			ImGui::RadioButton("ON", &shaderIndex, 1);
		}
		ImGui::End();
	}

	void handleKey(int key, int scancode, int action, int mods) override {
		// ��������������, ����� ������ ����������� ������ ��� ������� �� + � -
		Application::handleKey(key, scancode, action, mods);
		bool needsReset = false;

		if (key == GLFW_KEY_KP_SUBTRACT) {
			// ������ �� -, ���� �������� �����������
			if (detail > 3) { // ������ �� ������; 3 - ����������� ��������, ��� ������� ������ ����������
				detail--;
				needsReset = true;
			}
		}

		if (key == GLFW_KEY_KP_ADD) {
			// ������ �� +, ���� �������� �����������
			if (detail < 200) { // ������ �� ������; ���������� ���������� �� �������� � ������� ����������
				detail++; 
				needsReset = true;
			}
		}

		if (needsReset) { 
			// �������������� �����, ����� ����������� ������
			_moebiusStrip = makeMoebiusStrip(0.5f, detail);
			_moebiusStrip->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f)));
			draw();
		}
	}

	void draw() override
	{
		Application::draw();

		int width, height;
		glfwGetFramebufferSize(_window, &width, &height);

		glViewport(0, 0, width, height);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glPolygonMode(GL_FRONT_AND_BACK, polygonMode);

		//������������� ������.
		_shaders[shaderIndex]->use();

		//������������� ����� �������-����������
		_shaders[shaderIndex]->setMat4Uniform("viewMatrix", _camera.viewMatrix);
		_shaders[shaderIndex]->setMat4Uniform("projectionMatrix", _camera.projMatrix);

		//������ ���
		_shaders[shaderIndex]->setMat4Uniform("modelMatrix", _moebiusStrip->modelMatrix());
		

		_moebiusStrip->draw();


	}
};
int main(int argc, char** argv)
{
	SampleApplication app;
	app.start();

    return 0;
}