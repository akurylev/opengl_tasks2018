set(COMMON common)
include_directories(${COMMON})

set(SRC_FILES
        common/Application.cpp
        common/Camera.cpp
        common/Mesh.cpp
        common/ShaderProgram.cpp
        common/DebugOutput.cpp
        Main.cpp
        Maze.cpp
        )

MAKE_TASK(492Boymel 1 "${SRC_FILES}")

#target_include_directories(${PROJECT_SOURCE_DIR}492Boymel PUBLIC . common)
