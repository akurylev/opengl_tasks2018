//
// Created by Alexander Boymel on 08.03.18.
//
#include "Maze.h"

const float EPS = 0.15;  // not to see through walls


MazeApplication::MazeApplication(std::string _mazeFilename, float _cellSize, bool _faceCamera) {
    cellSize = _cellSize;
    faceCamera = _faceCamera;
    cellCount = 0;
    std::ifstream fin(_mazeFilename);

    fin >> width >> height;
    maze.resize(width);
    for (size_t i = 0; i < width; ++i) {
        maze[i].resize(height);
        for (size_t j = 0; j < height; ++j) {
            fin >> maze[i][j];
        }
    }
    fin.close();

    normalMap = { // normals inside
            {'l', glm::vec3(1.0, 0.0, 0.0)},
            {'r', glm::vec3(-1.0, 0.0, 0.0)},
            {'n', glm::vec3(0.0, 1.0, 0.0)},
            {'f', glm::vec3(0.0, -1.0, 0.0)},
            {'b', glm::vec3(0.0, 0.0, 1.0)},
            {'t', glm::vec3(0.0, 0.0, -1.0)}
    };
}


/*
walls:
  l
n   f
  r
b - bottom
t - top
l - left
r - right
n - near
f - far
*/
std::vector<char> MazeApplication::getWalls(int i, int j) {
    std::vector<char> walls;
    walls.push_back('b');
    walls.push_back('t');

    int cur = maze[i][j];

    if (cur == 0) {
        if ((i > 0) && (maze[i - 1][j] != cur))
            walls.push_back('l');
        if ((i < width - 1) && (maze[i + 1][j] != cur))
            walls.push_back('r');
        if ((j > 0) && (maze[i][j - 1] != cur))
            walls.push_back('n');
        if ((j < height - 1) && (maze[i][j + 1] != cur))
            walls.push_back('f');

        if (i == 0)
            walls.push_back('l');
        if (i == width - 1)
            walls.push_back('r');
        if (j == 0)
            walls.push_back('n');
        if (j == height - 1)
            walls.push_back('f');
    }

    return walls;
}


void MazeApplication::makeScene() {
    Application::makeScene();

    for (size_t i = 0; i < width; ++i) {
        for (size_t j = 0; j < height; ++j) {
            std::vector<char> walls = getWalls(i, j);
//            for (auto c: walls) {
//                std::cout << c;
//            }
//            std::cout << ' ';
            if (walls.size() > 0) {
                addCell(i, j, walls);
            }
        }
//        std::cout << std::endl;
    }

    //camera here
//    faceCameraPtr = std::make_shared<FreeCameraMover>();
    faceCameraPtr = std::make_shared<MazeFreeCameraMover>(this);
    orbitCameraPtr = std::make_shared<OrbitCameraMover>();
    if (faceCamera) {
        _cameraMover = faceCameraPtr;
    } else {
        _cameraMover = orbitCameraPtr;
    }

    shader = std::make_shared<ShaderProgram>("492BoymelData/shaders/shaderNormal.vert", "492BoymelData/shaders/shader.frag");
}


void MazeApplication::draw() {
    Application::draw();

    int _width, _height;
    glfwGetFramebufferSize(_window, &_width, &_height);

    glViewport(0, 0, _width, _height);

    //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Подключаем шейдер
    shader->use();

    //Устанавливаем общие юниформ-переменные
    shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
    shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

    //Рисуем
    for (int i = 0; i < cellCount; ++i) {
        shader->setMat4Uniform("modelMatrix", cellsTranslations[i]);
        cellsMesh[i]->draw();
    }
}


void MazeApplication::handleKey(int key, int scancode, int action, int mods)
{
    if (action == GLFW_PRESS)
    {
        if (key == GLFW_KEY_ESCAPE)
        {
            glfwSetWindowShouldClose(_window, GL_TRUE);
        }
        if (key == GLFW_KEY_F) {
//            _cameraMover = std::make_shared<FreeCameraMover>();
            _cameraMover = faceCameraPtr;
            std::cout << "face" << std::endl;
        }
        if (key == GLFW_KEY_O) {
//            _cameraMover = std::make_shared<OrbitCameraMover>();
            _cameraMover = orbitCameraPtr;
            std::cout << "orbit" << std::endl;
        }
    }
    _cameraMover->handleKey(_window, key, scancode, action, mods);
}


void MazeApplication::fillCellVertices(int i, int j, std::vector<glm::vec3> & cellVertices) {
    float half = cellSize / 2;
    float x = cellSize * i + half;
    float y = cellSize * j + half;

    /*
     * 0  1
     * 3  2
     */
    cellVertices.emplace_back(glm::vec3(x - half, y - half, 0.));
    cellVertices.emplace_back(glm::vec3(x - half, y + half, 0.));
    cellVertices.emplace_back(glm::vec3(x + half, y + half, 0.));
    cellVertices.emplace_back(glm::vec3(x + half, y - half, 0.));

    cellVertices.emplace_back(glm::vec3(x - half, y - half, cellSize));
    cellVertices.emplace_back(glm::vec3(x - half, y + half, cellSize));
    cellVertices.emplace_back(glm::vec3(x + half, y + half, cellSize));
    cellVertices.emplace_back(glm::vec3(x + half, y - half, cellSize));

    std::cout << "New Cube " << i << ',' << j << std::endl;
}


std::vector<glm::vec3> MazeApplication::getWallVertices(char type,
                                                        const std::vector<glm::vec3> & cellVertices) {
    std::vector<glm::vec3> wallVertices;
    switch (type) {
        case 'b':
            wallVertices.push_back(cellVertices[0]);
            wallVertices.push_back(cellVertices[3]);
            wallVertices.push_back(cellVertices[2]);
            wallVertices.push_back(cellVertices[1]);
            break;
        case 't':
            wallVertices.push_back(cellVertices[4]);
            wallVertices.push_back(cellVertices[5]);
            wallVertices.push_back(cellVertices[6]);
            wallVertices.push_back(cellVertices[7]);
            break;
        case 'l':
            wallVertices.push_back(cellVertices[0]);
            wallVertices.push_back(cellVertices[1]);
            wallVertices.push_back(cellVertices[5]);
            wallVertices.push_back(cellVertices[4]);
            break;
        case 'r':
            wallVertices.push_back(cellVertices[2]);
            wallVertices.push_back(cellVertices[3]);
            wallVertices.push_back(cellVertices[7]);
            wallVertices.push_back(cellVertices[6]);
            break;
        case 'n':
            wallVertices.push_back(cellVertices[0]);
            wallVertices.push_back(cellVertices[4]);
            wallVertices.push_back(cellVertices[7]);
            wallVertices.push_back(cellVertices[3]);
            break;
        case 'f':
            wallVertices.push_back(cellVertices[1]);
            wallVertices.push_back(cellVertices[2]);
            wallVertices.push_back(cellVertices[6]);
            wallVertices.push_back(cellVertices[5]);
            break;
    }
    return wallVertices;
}


MeshPtr MazeApplication::makeCellMesh(int i, int j, std::vector<char> walls) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> cellVertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texCoords;

    fillCellVertices(i, j, cellVertices);

    for (char type: walls) {
        std::vector<glm::vec3> wallVertices = getWallVertices(type, cellVertices);
        //1 triangle
        vertices.push_back(wallVertices[0]);
        vertices.push_back(wallVertices[1]);
        vertices.push_back(wallVertices[2]);
        //2 triangle
        vertices.push_back(wallVertices[2]);
        vertices.push_back(wallVertices[3]);
        vertices.push_back(wallVertices[0]);

        for (size_t k = 0; k < 6; ++k) {
            normals.push_back(normalMap[type]);
        }

        texCoords.emplace_back(glm::vec2(0.0, 0.0));
        texCoords.emplace_back(glm::vec2(1.0, 0.0));
        texCoords.emplace_back(glm::vec2(1.0, 1.0));

        texCoords.emplace_back(glm::vec2(1.0, 1.0));
        texCoords.emplace_back(glm::vec2(0.0, 1.0));
        texCoords.emplace_back(glm::vec2(0.0, 0.0));

    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texCoords.size() * sizeof(float) * 2, texCoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

//    std::cout << "Cube is created with " << vertices.size() << " vertices" << std::endl;

    return mesh;
}


void MazeApplication::addCell(int i, int j, const std::vector<char> & walls) {
    ++cellCount;
    cellsMesh.push_back(makeCellMesh(i, j, walls));
    cellsTranslations.push_back(glm::mat4(1.0f));
}


bool MazeApplication::checkCell(int i, int j, float x, float y, int pos_i, int pos_j) {
    if ((i < 0) || (i >= width) || (j < 0) || (j >= height) || maze[i][j] == 1)
        return true;
    if ((i == pos_i) && (j == pos_j))
        return true;

    float xBorder = cellSize * (i);
    if (pos_i > i)
        xBorder += cellSize;

    float yBorder = cellSize * j;
    if (pos_j > j)
        yBorder += cellSize;

    return sqrt(pow(x - xBorder, 2) * pow(i - pos_i, 2) + pow(y - yBorder, 2) * pow(j - pos_j, 2)) > EPS;
}


bool MazeApplication::possibleMove(glm::vec3 move) {
    if ((move[2] < 0) || (move[2] > cellSize))
        return true;
    else {
        int i = static_cast<int>(move[0] / cellSize);
        int j = static_cast<int>(move[1] / cellSize);

        float x = move[0];
        float y = move[1];

        if (x < 0)
            i -= 1;
        if (y < 0)
            j -= 1;
        for (int new_i = i - 1; new_i <= i + 1; ++new_i)
            for (int new_j = j - 1; new_j <= j + 1; ++new_j) {
//                std::cout << "log " << i << ',' << j << ' ' << x << ',' << y << ' ' << new_i << ',' << new_j << std::endl;
//                std::cout << checkCell(new_i, new_j, x, y, i, j) << std::endl;
                if (!checkCell(new_i, new_j, x, y, i, j))
                    return false;
            }
        return true;
    }

}


float MazeApplication::getCellSize() {
    return cellSize;
}


//-------------------------------------------------

MazeFreeCameraMover::MazeFreeCameraMover(MazeApplication* mazeApp) :
        CameraMover(),
        _pos(-2.0f, -2.0f, mazeApp->getCellSize() / 2),
        maze(mazeApp)
{
    //Нам нужно как-нибудь посчитать начальную ориентацию камеры
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void MazeFreeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{

}

void MazeFreeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - _oldXPos;
        double dy = ypos - _oldYPos;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.005), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.005), upDir);
    }

    _oldXPos = xpos;
    _oldYPos = ypos;
}

void MazeFreeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeFreeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.0f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        if (maze->possibleMove(_pos + forwDir * speed * static_cast<float>(dt)))
            _pos += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        if (maze->possibleMove(_pos - forwDir * speed * static_cast<float>(dt)))
            _pos -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        if (maze->possibleMove(_pos - rightDir * speed * static_cast<float>(dt)))
            _pos -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        if (maze->possibleMove(_pos + rightDir * speed * static_cast<float>(dt)))
            _pos += rightDir * speed * static_cast<float>(dt);
    }

    //-----------------------------------------
    _pos[2] = maze->getCellSize() / 2;

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}
