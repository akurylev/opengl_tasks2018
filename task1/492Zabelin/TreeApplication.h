#pragma once

#include <iostream>
#include <vector>

#include <Application.hpp>
#include <Common.h>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include "LSystem.h"


class TreeApplication : public Application {
    ShaderProgramPtr shader;
    std::vector<ConeParams> conesParams;
    std::vector<MeshPtr> cones;
public:
    void setupLSystem(
        LSystem& system,
        uint16_t numIterations
    );
    void makeScene() override;
    void draw() override;
};
