/**
Использование текстуры в качестве коэффициента отражения света
*/

#version 330

uniform sampler2D diffuseTexture;
uniform sampler2D normalMapTexture;
uniform mat4 viewMatrix;

struct LightInfo
{
    //направление на источник света в мировой системе координат (для направленного источника)
    vec3 dir;
    //цвет и интенсивность окружающего света
    vec3 La;
    //цвет и интенсивность диффузного света
    vec3 Ld;
    //цвет и интенсивность бликого света
    vec3 Ls;
};

uniform LightInfo light;

struct MaterialInfo
{
    //коэффициент отражения окружающего света
    vec3 Ka;
    //коэффициент отражения диффузного света
    vec3 Kd;
    //коэффициент отражения бликого света
    vec3 Ks;
};
uniform MaterialInfo material;


//нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec3 normal;

//координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec3 position;

//текстурные координаты (интерполирована между вершинами треугольника)
in vec2 textureCoords;

//выходной цвет фрагмента
out vec4 fragColor;

const float shininess = 64.0;

void main()
{
	vec4 diffuseColor4 = texture(diffuseTexture, textureCoords);
    vec3 diffuseColor = diffuseColor4.rgb;
    float alpha = diffuseColor4.a;
	vec3 normalMap = texture(normalMapTexture, textureCoords).rgb;

    //нормализуем нормаль после интерполяции
	vec3 normedNormal = normalize(normal);

    //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
	vec3 viewDirection = normalize(-position.xyz);

    //направление на источник света - из мировой в систему координат камеры
    vec4 lightDirCamSpace = viewMatrix * normalize(vec4(light.dir, 0.0));

    //скалярное произведение (косинус)
	float NdotL = abs(dot(normedNormal, lightDirCamSpace.xyz));

	vec3 color = diffuseColor * (light.La * material.Ka + light.Ld * material.Kd * NdotL);

	if (NdotL > 0.0)
	{
		//биссектриса между направлениями на камеру и на источник света
        vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection);

		//интенсивность бликового освещения по Блинну
        float blinnTerm = max(dot(normedNormal, halfVector), 0.0);

		//регулируем размер блика
        blinnTerm = pow(blinnTerm, shininess);
		color += light.Ls * material.Ks * blinnTerm;
	}

	fragColor = vec4(color, alpha);
}
