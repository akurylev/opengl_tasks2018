#include "Mesh.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <iostream>
#include <vector>

glm::vec3 getBreatherSurfaceVertice(float b, float u, float v) {
    float r = 1 - b * b;
    float w = sqrt(r);
    float denom = b * ((w * cosh(b * u))*(w * cosh(b * u)) + (b * sin(w * v))*(b * sin(w * v)));

    float x = -u + (2 * r * cosh(b * u) * sinh(b * u) / denom);
    float y = 2 * w * cosh(b * u) * (-(w * cos(v) * cos(w * v)) - (sin(v) * sin(w * v))) / denom;
    float z = 2 * w * cosh(b * u) * (-(w * sin(v) * cos(w * v)) + (cos(v) * sin(w * v))) / denom;

    return glm::vec3(x, y, z);
}

glm::vec3 getNormal(float b, float u, float v) {
    float r = 1 - b * b;
    float w = sqrt(r);
    float denom = b * ((w * cosh(b * u))*(w * cosh(b * u)) + (b * sin(w * v))*(b * sin(w * v)));

    float nx = 4 * b * w * w / (denom * denom) * sinh(b * u) * cosh(b * u) * ((w * w * sin(v) * sin(w * v) - sin(v) * sin(v * w)) * (- w * cos(v) * cos(v * w) - sin(v) * sin(w * v)) - (w * w - 1) * cos(v) * sin(v * w) * (- w * cos(v * w) * sin(v) + cos(v) * sin(v * w)));
    float ny = - 2 * w * cosh(b * u) / denom * (w * w - 1) * sin(v) * sin(v * w) * (2 * b * r / denom * (sinh(b * u) * sinh(b * u) + cosh(b * u) * cosh(b * u)) - 1);
    float nz = 2 * w * cosh(b * u) / denom * (w * w - 1) * cos(v) * sin(v * w) * (2 * b * r / denom * (sinh(b * u) * sinh(b * u) + cosh(b * u) * cosh(b * u)) - 1);
    float n0 = sqrt(nx * nx + ny * ny + nz * nz);
    return glm::vec3(nx / n0, ny / n0, nz / n0);
}

MeshPtr makeBreatherSurface(float b, unsigned int N)
{
    unsigned int M = N / 2;

    //Зададим 2 вершинных атрибута: координаты вершины и нормаль к поверхности в вершине

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    float u_min = -14;
    float u_max = 14;

    float v_min = -37.4;
    float v_max = 38;

    for (unsigned int i = 0; i < M; i++)
    {
        float u_1 = u_min + (u_max - u_min) * i / M;
        float u_2 = u_min + (u_max - u_min) * (i + 1) / M;

        for (unsigned int j = 0; j < N; j++)
        {
            float v_1 = v_min + (v_max - v_min) * j / N;
            float v_2 = v_min + (v_max - v_min) * (j + 1) / N;

            vertices.push_back(getBreatherSurfaceVertice(b, u_1, v_1));
            vertices.push_back(getBreatherSurfaceVertice(b, u_2, v_2));
            vertices.push_back(getBreatherSurfaceVertice(b, u_2, v_1));

            normals.push_back(getNormal(b, u_1, v_1));
            normals.push_back(getNormal(b, u_2, v_2));
            normals.push_back(getNormal(b, u_2, v_1));

            vertices.push_back(getBreatherSurfaceVertice(b, u_1, v_1));
            vertices.push_back(getBreatherSurfaceVertice(b, u_1, v_2));
            vertices.push_back(getBreatherSurfaceVertice(b, u_2, v_2));

            normals.push_back(getNormal(b, u_1, v_1));
            normals.push_back(getNormal(b, u_1, v_2));
            normals.push_back(getNormal(b, u_2, v_2));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Breathe Surface is created with " << vertices.size() << " vertices\n";

    return mesh;
}

MeshPtr makeSurface(float size)
{
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    //Oxy
    vertices.push_back(glm::vec3(-size, size, 0));
    vertices.push_back(glm::vec3(size, size, 0));
    vertices.push_back(glm::vec3(size, -size, 0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    //Oxy
    vertices.push_back(glm::vec3(-size, size, 0));
    vertices.push_back(glm::vec3(size, -size, 0));
    vertices.push_back(glm::vec3(-size, -size, 0));

    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));
    normals.push_back(glm::vec3(0.0, 0.0, 1.0));

    //Oxz
    vertices.push_back(glm::vec3(-size, 0, size));
    vertices.push_back(glm::vec3(size, 0, size));
    vertices.push_back(glm::vec3(size, 0, -size));

    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));

    //Oxz
    vertices.push_back(glm::vec3(-size, 0, size));
    vertices.push_back(glm::vec3(size, 0, -size));
    vertices.push_back(glm::vec3(-size, 0, -size));

    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));
    normals.push_back(glm::vec3(0.0, 1.0, 0.0));

    //Oyz
    vertices.push_back(glm::vec3(0, -size, size));
    vertices.push_back(glm::vec3(0, size, size));
    vertices.push_back(glm::vec3(0, size, -size));

    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));

    //Oyz
    vertices.push_back(glm::vec3(0, -size, size));
    vertices.push_back(glm::vec3(0, size, -size));
    vertices.push_back(glm::vec3(0, -size, -size));

    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));
    normals.push_back(glm::vec3(1.0, 0.0, 0.0));

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());


    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Surface is created with " << vertices.size() << " vertices\n";

    return mesh;
}