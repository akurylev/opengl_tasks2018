#version 330 core
layout (location = 0) in vec3 position;

out vec3 Position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform float size;
uniform sampler2D ourTexture;

void main()
{
    	vec2 TexCoord = position.xz * vec2(1/size, -1/size) + vec2(0.5, -0.5);
	vec4 color = texture(ourTexture, TexCoord);
	Position = vec3(position.x, color.x, position.z);
	gl_Position = projection * view * model * vec4(Position.x, Position.y * 0.1f, Position.z, 1.0f);
} 

