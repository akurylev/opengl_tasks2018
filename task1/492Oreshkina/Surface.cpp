#include "common/Application.hpp"
#include "common/Mesh.hpp"
#include "common/ShaderProgram.hpp"

#include <iostream>

class SampleApplication : public Application {
public:
    MeshPtr _bottle;

    ShaderProgramPtr _shader, _shaderPolygon;

    unsigned int _N = 30;

    const unsigned int kMaxn = 400;
    const unsigned int kMinn = 6;


    void makeScene() override
    {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        // Определяем объект отрисовки
        _bottle = makeBottle(2.0f, _N);
        _bottle->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, -1.0f, 0.5f)));

        //Создаем шейдерную программу
        _shader = std::make_shared<ShaderProgram>("492OreshkinaData/shaderNormal.vert",
                                                  "492OreshkinaData/shader.frag");

        _shaderPolygon = std::make_shared<ShaderProgram>("492OreshkinaData/shaderPolygon.vert",
                                                        "492OreshkinaData/shader.frag");
    }

    void handleKey(int key, int scancode, int action, int mods) override
    {
        Application::handleKey(key, scancode, action, mods);

        if (action == GLFW_PRESS)
        {
            // Изменение количества точек для отрисовки бутылки
            if (key == GLFW_KEY_MINUS && (mods & GLFW_MOD_SHIFT))
            {
                _N *= 0.9;
                _N = std::max(kMinn, _N);
                _bottle = makeBottle(2.0f, _N);
            }
            else if (key == GLFW_KEY_EQUAL && (mods & GLFW_MOD_SHIFT))
            {
                _N *= (0.1 * _N < 1)? 1.2 : 1.1;
                _N = std::min(kMaxn, _N);
                _bottle = makeBottle(2.0f, _N);
            }
        }
        Application::update();
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
        {
            if (_bottle) {
                ImGui::Text("Number of vertices: %d", _bottle->getVertexCount());
            }
        }
        ImGui::End();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


        //Устанавливаем шейдер.
        _shader->use();

        //Устанавливаем общие юниформ-переменные
        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем первый меш
        _shader->setMat4Uniform("modelMatrix", _bottle->modelMatrix());
        _bottle->draw();


        glEnable(GL_POLYGON_OFFSET_LINE);
        glPolygonOffset(-1.0,-1.0);

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

        _shaderPolygon->use();
        _shaderPolygon->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shaderPolygon->setMat4Uniform("projectionMatrix", _camera.projMatrix);
        _shaderPolygon->setMat4Uniform("modelMatrix", _bottle->modelMatrix());
        _bottle->draw();

        //glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

        glDisable(GL_POLYGON_OFFSET_LINE);
    }
};

int main() {
    SampleApplication app;
    app.start();

    return 0;
}