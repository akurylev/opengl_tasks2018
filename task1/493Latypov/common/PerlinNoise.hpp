#pragma once

#include <cmath>
#include <random>
#include <vector>

struct Vector2D {
    float x;
    float y;

    Vector2D(float x_, float y_) : x(x_), y(y_) {}
};

class PerlinNoise {
public:
    explicit PerlinNoise(int num_of_octaves = 1, float persistence = 0.5f, int seed = 0);

    float perlinNoise2D(float fx, float fy);

private:
    float cosineInterpolation(float a, float b, float x);

    Vector2D getPseudoRandomGradVector(int x, int y);

    float dot(const Vector2D &x, const Vector2D &y);

    float noise(float fx, float fy);

    std::vector<int> permutation_table_;
    int num_of_octaves_;
    float persistence_;
};