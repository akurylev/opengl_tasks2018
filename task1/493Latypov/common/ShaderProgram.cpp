#include "ShaderProgram.hpp"

#include <iostream>
#include <fstream>

void Shader::createFromFile(const std::string &file_path) {
    std::ifstream shaderFile(file_path.c_str());
    if (shaderFile.fail()) {
        std::cerr << "Failed to load shader file " << file_path << std::endl;
        exit(1);
    }
    std::string shaderFileContent((std::istreambuf_iterator<char>(shaderFile)),
                                  (std::istreambuf_iterator<char>()));
    shaderFile.close();
    createFromString(shaderFileContent);
}

void Shader::createFromString(const std::string &text) {
    const char *shaderText = text.c_str();
    glShaderSource(id_, 1, &shaderText, nullptr);
    glCompileShader(id_);
    int status = -1;
    glGetShaderiv(id_, GL_COMPILE_STATUS, &status);
    if (status != GL_TRUE) {
        GLint errorLength;
        glGetShaderiv(id_, GL_INFO_LOG_LENGTH, &errorLength);
        std::vector<char> errorMessage;
        errorMessage.resize(static_cast<unsigned long>(errorLength));
        glGetShaderInfoLog(id_, errorLength, nullptr, errorMessage.data());
        std::cerr << "Failed to compile the shader:\n" << errorMessage.data() << std::endl;
        exit(1);
    }
}

void ShaderProgram::createProgram(const std::string &vert_file_path,
                                  const std::string &frag_file_path) {
    std::shared_ptr<Shader> vs = std::make_shared<Shader>(GL_VERTEX_SHADER);
    vs->createFromFile(vert_file_path);
    attachShader(vs);
    std::shared_ptr<Shader> fs = std::make_shared<Shader>(GL_FRAGMENT_SHADER);
    fs->createFromFile(frag_file_path);
    attachShader(fs);
    linkProgram();
}

void ShaderProgram::attachShader(const std::shared_ptr<Shader> &shader) {
    glAttachShader(program_id_, shader->id());
    shaders_.push_back(shader);
}

void ShaderProgram::linkProgram() {
    glLinkProgram(program_id_);
    int status = -1;
    glGetProgramiv(program_id_, GL_LINK_STATUS, &status);
    if (status != GL_TRUE) {
        GLint errorLength;
        glGetProgramiv(program_id_, GL_INFO_LOG_LENGTH, &errorLength);
        std::vector<char> errorMessage;
        errorMessage.resize(static_cast<unsigned long>(errorLength));
        glGetProgramInfoLog(program_id_, errorLength, nullptr, errorMessage.data());
        std::cerr << "Failed to link the program:\n" << errorMessage.data() << std::endl;
        exit(1);
    }
}
