#include "PerlinNoise.hpp"

PerlinNoise::PerlinNoise(int num_of_octaves, float persistence, int seed) {
    persistence_ = persistence;
    num_of_octaves_ = num_of_octaves;
    permutation_table_ = std::vector<int>(1024);
    srand(static_cast<unsigned int>(seed));
    for (unsigned int i = 0; i < 1024; ++i) {
        permutation_table_[i] = rand() % 256; // TODO C++11
    }
}

float PerlinNoise::cosineInterpolation(float a, float b, float x) {
    float ft = x * 3.1415927f;
    auto f = (1.f - std::cos(ft)) * .5f;
    return a * (1.f - f) + b * f;
}

Vector2D PerlinNoise::getPseudoRandomGradVector(int x, int y) {
    auto v = (int) (((x * 1836311903) ^ (y * 2971215073) + 4807526976) & 1023);
    v = permutation_table_[v] & 3;
    switch (v) {
        case 0:
            return {1.f, 0.f};
        case 1:
            return {-1.f, 0.f};
        case 2:
            return {0.f, 1.f};
        default:
            return {0.f, -1.f};
    }
}

float PerlinNoise::dot(const Vector2D &x, const Vector2D &y) {
    return x.x * y.x + x.x * y.y;
}

float PerlinNoise::noise(float fx, float fy) {
    auto left = static_cast<int>(std::floor(fx));
    auto top = static_cast<int>(std::floor(fy));
    float frac_x = fx - left;
    float frac_y = fy - top;

    Vector2D top_left_grad = getPseudoRandomGradVector(left, top);
    Vector2D top_right_grad = getPseudoRandomGradVector(left + 1, top);
    Vector2D bottom_left_grad = getPseudoRandomGradVector(left, top + 1);
    Vector2D bottom_right_grad = getPseudoRandomGradVector(left + 1, top + 1);

    Vector2D distance_to_top_left = Vector2D(frac_x, frac_y);
    Vector2D distance_to_top_right = Vector2D(frac_x - 1, frac_y);
    Vector2D distance_to_bottom_left = Vector2D(frac_x, frac_y - 1);
    Vector2D distance_to_bottom_right = Vector2D(frac_x - 1, frac_y - 1);

    float tx1 = dot(distance_to_top_left, top_left_grad);
    float tx2 = dot(distance_to_top_right, top_right_grad);
    float bx1 = dot(distance_to_bottom_left, bottom_left_grad);
    float bx2 = dot(distance_to_bottom_right, bottom_right_grad);

    float tx = cosineInterpolation(tx1, tx2, frac_x);
    float bx = cosineInterpolation(bx1, bx2, frac_x);
    return cosineInterpolation(tx, bx, frac_y);
}

float PerlinNoise::perlinNoise2D(float fx, float fy) {
    float amplitude = 1.0f;
    float max = 0.0f;
    float result = 0.0f;

    for (int i = num_of_octaves_; i > 0; i--) {
        max += amplitude;
        result += noise(fx, fy) * amplitude;
        amplitude *= persistence_;
        fx *= 2;
        fy *= 2;
    }
    return result / max;
}