#include "Application.hpp"
#include "Mesh.hpp"
#include "ShaderProgram.hpp"
#include "PerlinNoise.hpp"

class SurfaceApplication : public Application {
public:
    SurfaceApplication();

    void makeScene() override;

    void draw() override;

protected:
    MeshPtr mesh_;
    std::shared_ptr<ShaderProgram> shader_;
};
