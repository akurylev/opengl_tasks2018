#include <Application.hpp>
#include <Mesh.hpp>
#include <ShaderProgram.hpp>

#include <iostream>
#include <vector>
#include <algorithm>

template<typename T>
class Mat {
private:
    const int m;
    T* data;
public:
    Mat(int n, int m) : m(m), data(new T[n * m]) {}
    ~Mat() { delete[] data; }
    T* operator[](int i) { return &data[i*m]; }
    const T* operator[](int i) const { return &data[i*m]; }
};

MeshPtr makeRelief() {
    const unsigned int M = 300;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    Mat<double> z0(M+1, M+1);
    Mat<double> z(M+1, M+1);

    for (unsigned int i = 0; i < M+1; ++i) {
        for (unsigned int j = 0; j < M+1; ++j) {
            z0[i][j] = static_cast<double>(rand()) / static_cast<double>(RAND_MAX);
            z[i][j] = z0[i][j];
        }
    }

    const double kernel[3][3] = {{0.077847,0.123317,0.077847},{0.123317,0.195346,0.123317},{0.077847,0.123317,0.077847}};

    for (unsigned int pass = 0; pass < 10; ++pass) {
        for (unsigned int i = 1; i < M; ++i) {
            for (unsigned int j = 1; j < M; ++j) {
                double s = 0;
                s += z0[i-1][j-1] * kernel[0][0];
                s += z0[i-1][j+0] * kernel[0][1];
                s += z0[i-1][j+1] * kernel[0][2];
                s += z0[i+0][j-1] * kernel[1][0];
                s += z0[i+0][j+0] * kernel[1][1];
                s += z0[i+0][j+1] * kernel[1][2];
                s += z0[i+1][j-1] * kernel[2][0];
                s += z0[i+1][j+0] * kernel[2][1];
                s += z0[i+1][j+1] * kernel[2][2];
                z[i][j] = s;
            }
        }

        for (unsigned int i = 0; i < M+1; ++i)
            for (unsigned int j = 0; j < M+1; ++j)
                z0[i][j] = z[i][j];
    }

    for (unsigned int i = 0; i < M+1; ++i) {
        z[i][0] = 0;
        z[0][i] = 0;
        z[i][M] = 0;
        z[M][i] = 0;
    }

    Mat<glm::vec3> norms(M+1, M+1);

    for (unsigned int i = 0; i < M; ++i) {
        for (unsigned int j = 0; j < M; ++j) {
            const double x0 = -1.0 + 5.0 * i / M;
            const double y0 = -1.0 + 5.0 * j / M;
            const double x1 = -1.0 + 5.0 * (i + 1) / M;
            const double y1 = -1.0 + 5.0 * (j + 1) / M;

            vertices.push_back(glm::vec3(x0, y0, z[i][j]));
            vertices.push_back(glm::vec3(x1, y0, z[i+1][j]));
            vertices.push_back(glm::vec3(x0, y1, z[i][j+1]));

            glm::vec3 n1 = -glm::normalize(glm::cross(vertices[vertices.size()-1] - vertices[vertices.size()-3], vertices[vertices.size()-2] - vertices[vertices.size()-3]));

            norms[i][j] += n1;
            norms[i+1][j] += n1;
            norms[i][j+1] += n1;

            vertices.push_back(glm::vec3(x0, y1, z[i][j+1]));
            vertices.push_back(glm::vec3(x1, y1, z[i+1][j+1]));
            vertices.push_back(glm::vec3(x1, y0, z[i+1][j]));

            n1 = glm::normalize(glm::cross(vertices[vertices.size()-1] - vertices[vertices.size()-3], vertices[vertices.size()-2] - vertices[vertices.size()-3]));

            norms[i][j+1] += n1;
            norms[i+1][j+1] += n1;
            norms[i+1][j] += n1;
        }
    }

    for (unsigned int i = 0; i <= M; ++i)
        for (unsigned int j = 0; j <= M; ++j)
            norms[i][j] = glm::normalize(norms[i][j]);

    for (unsigned int i = 0; i < M; ++i) {
        for (unsigned int j = 0; j < M; ++j) {
            normals.push_back(norms[i][j]);
            normals.push_back(norms[i+1][j]);
            normals.push_back(norms[i][j+1]);

            normals.push_back(norms[i][j+1]);
            normals.push_back(norms[i+1][j+1]);
            normals.push_back(norms[i+1][j]);
        }
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    return mesh;
}

class TargetApplication : public Application {
public:
    MeshPtr _relief;

    ShaderProgramPtr _shader;

    void makeScene() override {
        Application::makeScene();
        _cameraMover = std::make_shared<FreeCameraMover>();
        _relief = makeRelief();
        _shader = std::make_shared<ShaderProgram>("491ZaripovData/shader.vert", "491ZaripovData/shader.frag");
    }

    void update() override {
        Application::update();
    }

    void draw() override
    {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _shader->setMat4Uniform("modelMatrix", _relief->modelMatrix());
        _relief->draw();
    }
};

int main() {
    TargetApplication app;
    app.start();

    return 0;
}
