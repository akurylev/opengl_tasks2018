set(MYDIR task1/497Zotov)
include_directories(".")

set(SRC_FILES
    Application.cpp
    Camera.cpp
    Mesh.cpp
    ShaderProgram.cpp
    DebugOutput.cpp
    Main.cpp
    PerlinNoise.cpp
)

set(HEADER_FILES
    Application.hpp
    Camera.hpp
    Mesh.hpp
    ShaderProgram.hpp
    DebugOutput.h
    PerlinNoise.h
)

set(SHADER_FILES
    shaders/shader.frag
    shaders/shaderNormal.vert
    shaders/shaderHeight.vert
    shaders/shaderWhite.vert
)

source_group("Shaders" FILES ${SHADER_FILES})

MAKE_TASK(497Zotov 1 "${SRC_FILES}")
