#include "cone.hpp"

void normalize(glm::vec3& vector) {
    vector = glm::vec3(
        vector[0] / glm::length(vector),
        vector[1] / glm::length(vector),
        vector[2] / glm::length(vector)
    );
}

void push_segment(
    std::vector<glm::vec3>& vertices,
    std::vector<glm::vec3>& normals,
    float x_top_begin, float x_top_end,
    float y_top_begin, float y_top_end,
    float x_bottom_begin, float x_bottom_end,
    float y_bottom_begin, float y_bottom_end,
    float height
) {
    vertices.push_back(glm::vec3(x_bottom_begin, y_bottom_begin, 0));
    vertices.push_back(glm::vec3(x_bottom_end, y_bottom_end, 0));
    vertices.push_back(glm::vec3(0, 0, 0));
    for (int i = 0; i < 3; ++i) {
        normals.push_back(glm::vec3(0, 0, -1));
    }
    vertices.push_back(glm::vec3(x_top_begin, y_top_begin, height));
    vertices.push_back(glm::vec3(x_top_end, y_top_end, height));
    vertices.push_back(glm::vec3(0, 0, height));
    for (int i = 0; i < 3; ++i) {
        normals.push_back(glm::vec3(0, 0, 1));
    }

    glm::vec3 normal = glm::cross(
        glm::vec3(
            x_top_begin - x_bottom_begin,
            y_top_begin - y_bottom_begin,
            height
        ),
        glm::vec3(
            x_bottom_end - x_bottom_begin,
            y_bottom_end - y_bottom_begin,
            0
        )
    );
    normalize(normal);

    // Нормаль везде одинаковая. Всего 6 = 3 * 2 точек (2 треугольника)
    for (int i = 0; i < 6; ++i) {
        normals.push_back(normal);
    }

    vertices.push_back(glm::vec3(
        x_top_begin,
        y_top_begin,
        height
    ));
    vertices.push_back(glm::vec3(
        x_bottom_begin,
        y_bottom_begin,
       0 
    ));
    vertices.push_back(glm::vec3(
        x_bottom_end,
        y_bottom_end,
       0 
    ));

    vertices.push_back(glm::vec3(
        x_top_begin,
        y_top_begin,
        height
    ));
    vertices.push_back(glm::vec3(
        x_top_end,
        y_top_end,
        height
    ));
    vertices.push_back(glm::vec3(
        x_bottom_end,
        y_bottom_end,
       0 
    ));
}


void push_rect(
    std::vector<glm::vec3>& vertices,
    std::vector<glm::vec3>& normals,
    float x_begin, float x_end,
    float y_begin, float y_end,
    float height
) {
    vertices.push_back(glm::vec3(x_begin, y_begin, 0));
    vertices.push_back(glm::vec3(x_end, y_end, 0));
    vertices.push_back(glm::vec3(0, 0, 0));
    for (int i = 0; i < 3; ++i) {
        normals.push_back(glm::vec3(0, 0, -1));
    }
    vertices.push_back(glm::vec3(x_begin, y_begin, height));
    vertices.push_back(glm::vec3(x_end, y_end, height));
    vertices.push_back(glm::vec3(0, 0, height));
    for (int i = 0; i < 3; ++i) {
        normals.push_back(glm::vec3(0, 0, 1));
    }

    vertices.push_back(glm::vec3(x_begin, y_begin, 0));
    vertices.push_back(glm::vec3(x_begin, y_begin, height));
    vertices.push_back(glm::vec3(x_end, y_end, 0));

    vertices.push_back(glm::vec3(x_end, y_end, 0));
    vertices.push_back(glm::vec3(x_end, y_end, height));
    vertices.push_back(glm::vec3(x_begin, y_begin, height));

    for (int i = 0; i < 6; ++i) {
        glm::vec3 normal = glm::cross(
            glm::vec3(x_end - x_begin, y_end - y_begin, 0),
            glm::vec3(0, 0, 1)
        );
        normal = glm::vec3(
            normal[0] / glm::length(normal),
            normal[1] / glm::length(normal),
            normal[2] / glm::length(normal)
        );
        normals.push_back(normal);
    }
}


MeshPtr make_cone(float bottom_width, float top_width, float height) {
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;

    int steps_num = 50;
    float x_bottom = bottom_width / 2;
    float y_bottom = 0;
    float x_top = top_width / 2;
    float y_top = 0;
    const double PI = std::acos(-1);
    std::vector<float> x_begins_bottom;
    std::vector<float> y_begins_bottom;
    std::vector<float> x_ends_bottom;
    std::vector<float> y_ends_bottom;
    std::vector<float> x_begins_top;
    std::vector<float> y_begins_top;
    std::vector<float> x_ends_top;
    std::vector<float> y_ends_top;
    for (int i = 1; i <= steps_num; ++i) {
        x_begins_bottom.push_back(x_bottom);
        y_begins_bottom.push_back(y_bottom);
        x_bottom = bottom_width / 2 * std::cos(2.0 * PI * i / steps_num);
        y_bottom = bottom_width / 2 * std::sin(2.0 * PI * i / steps_num);
        x_ends_bottom.push_back(x_bottom);
        y_ends_bottom.push_back(y_bottom);

        x_begins_top.push_back(x_top);
        y_begins_top.push_back(y_top);
        x_top = top_width / 2 * std::cos(2.0 * PI * i / steps_num);
        y_top = top_width / 2 * std::sin(2.0 * PI * i / steps_num);
        x_ends_top.push_back(x_top);
        y_ends_top.push_back(y_top);
    }

    for (int i = 0; i < steps_num; ++i) {
        push_segment(
            vertices, normals,
            x_begins_top[i], x_ends_top[i],
            y_begins_top[i], y_ends_top[i],
            x_begins_bottom[i], x_ends_bottom[i],
            y_begins_bottom[i], y_ends_bottom[i],
            height
        );
    }

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "cone is created with " << vertices.size() << " vertices" << std::endl;

    return mesh;
}
