#pragma once

#include "application.hpp"
#include "mesh.hpp"
#include "shader_program.hpp"

#include <cmath>
#include <iostream>
#include <vector>

#include "cone.hpp"
#include "l_system.hpp"


class TreeApplication : public Application {
private:
    std::vector<MeshPtr> branches;
    ShaderProgramPtr shader;
    LSystem system;
    std::vector<std::pair<glm::vec3, float>> points;
public:
    void setup_LSystem(
        std::map<char, std::string> rules,
        std::string initialString,
        float rotate_angle,
        float step_distance,
        float distance_scale,
        float angle_scale,
        float start_width,
        float width_scale,
        int num_iterations
    ) {
        system = LSystem(
            rules, initialString,
            rotate_angle, step_distance,
            distance_scale, angle_scale,
            start_width, width_scale
        );
        system.execute_iterations(num_iterations);
        points = system.get_points();
    }

    MeshPtr make_branch(glm::vec3 first_point, glm::vec3 second_point, float bottom_width, float top_width) {
        if (second_point[2] < first_point[2]) {
            glm::vec3 tmp_point = first_point;
            first_point = second_point;
            second_point = tmp_point;
            float tmp_width = top_width;
            top_width = bottom_width;
            bottom_width = tmp_width;
        }
        //Создаем меш с цилиндром
        float distance = glm::distance(first_point, second_point);
        glm::vec3 projection(second_point[0], second_point[1], first_point[2]);
        glm::mat4 model_matrix;
        glm::mat4 translation = glm::translate(glm::mat4(1.0f), first_point);
        if (projection != first_point) {
            float sin = glm::distance(projection, first_point) / distance;
            float angle = std::asin(sin);
            glm::vec3 axis = glm::cross(glm::vec3(0, 0, 1), projection - first_point);
            model_matrix = glm::rotate(translation, angle, axis);
        } else {
            model_matrix = translation;
        }

        MeshPtr cone = make_cone(bottom_width, top_width, distance);
        cone->setModelMatrix(model_matrix);
        branches.push_back(cone);
    }

    void makeScene() override {
        Application::makeScene();

        _cameraMover = std::make_shared<FreeCameraMover>();

        for (auto it = points.begin(); it != points.end(); ++it) {
            // Сейчас тут надо перепутать y и z, не понятно почему.
            glm::vec3 start_point = it->first;
            float bottom_width = it->second;
            ++it;
            glm::vec3 end_point = it->first;
            float top_width = it->second;
            make_branch(start_point, end_point, bottom_width, top_width);
        }
        //Создаем шейдерную программу        
        shader = std::make_shared<ShaderProgram>("492KoshkinData/shaderNormal.vert", "492KoshkinData/shader.frag");
    }

    void draw() override {
        Application::draw();

        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Устанавливаем шейдер.
        shader->use();

        //Устанавливаем общие юниформ-переменные
        shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        //Рисуем мешы
        for (auto branch: branches) {
            shader->setMat4Uniform("modelMatrix", branch->modelMatrix());
            branch->draw();
        }
    }
};
